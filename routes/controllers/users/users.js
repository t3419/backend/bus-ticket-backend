const User = require('../../../models/User');
const Trip = require('../../../models/Trip')
const bcrypt = require('bcryptjs');
const sharp = require('sharp');
const Promise = require('bluebird')
const { sendRegisterEmail } = require("../../../services/email/sendRegisterEmail");
// const { promisify } = require('util');
// const comparePassword = promisify(bcrypt.compare);
// const jwtSign = promisify(jwt.sign);

//thu vien util cho phep viet bcrypt o dang promist mac dinh chi o dang callback
//phuong thuc promisify ho tro chuyen 1 callback thanh 1 promise

/**
 * @todo api for User
 */
module.exports.createUser = async (req, res, next) => {
  const user = new User(req.body);
  try {
    // create code random with 6 digit
    user.confirmCode = '';
    for (let i = 1; i <= 6; i++){
      user.confirmCode += Math.floor(Math.random() * 10); 
    }
    // set beginning time to code
    user.dateConfirmCode = Date.now();
    // set new user is non active
    user.isActive = false;

    console.log(user);
    // create new user
    await user.save();
    
    // send mail confirm to new user
    sendRegisterEmail(user);
    
    res.status(201).send({email: user.email, message: "Sign up success. Please confirm email"});
  } catch (e) {
    res.status(400).json(e);
  }
};

// user has to enter email and code
module.exports.confirmUser = async (req, res, next) => {
  const confirmCode = req.body.confirmCode;
  const user = await User.findByEmail(
    req.body.email
  );
  try {
    // get time code exist (millisecond)
    const timeLast = Date.now() - user.dateConfirmCode;

    console.log(timeLast);
    // if code is over 6 minutes request user get new code
    if (timeLast / 1000 > 6 * 60){
      return res.status(401).json({message: "Code is expired"});
    }else{
      // compare code if equal -> active user
      if (confirmCode == user.confirmCode){
        user.isActive = true;
        user.save();
        console.log(user);
        res.status(200).send({message: "Confirm success please login"});
      } else {
        return res.status(401).json({message: "Code is wrong"});
      }
    }
    
  } catch (e) {
    res.status(400).json(e);
  }
};

module.exports.resendConfirmCode = async (req, res, next) => {
  const user = await User.findByEmail(
    req.body.email
  );
  try{
    // create code random with 6 digit
    user.confirmCode = '';
    for (let i = 1; i <= 6; i++){
      user.confirmCode += Math.floor(Math.random() * 10); 
    }
    // set beginning time to code
    user.dateConfirmCode = Date.now();
    user.save();

    // re-send email
    sendRegisterEmail(user);

    res.status(200).json({message: 'Resend success'});
  } catch(e) {
    res.status(400).json(e);
  }
  
}
module.exports.getUsers = (req, res, next) => {
  User.find()
    .then((user) => {
      user = user.filter((user) => user.userType !== 'admin');
      return res.status(200).json(user);
    })
    .catch((err) => res.status(500).json(err));
};

module.exports.getUserById = async (req, res, next) => {
  res.send(req.user);
};

module.exports.deleteUserById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) throw new Error('User not found');

    await user.remove();
    return res.status(200).send({ message: 'Delete user successfully' });
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: e.message });
  }
};

module.exports.updateUserById = async (req, res, next) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ['fullName', 'email', 'phoneNumber', 'dayOfBirth'];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation)
    return res.status(400).send({ error: 'Invalid updates!' });

  try {
    updates.forEach((update) => (req.user[update] = req.body[update]));

    await req.user.save();
    res.status(200).send(req.user);
  } catch (e) {
    res.status(400).send(e);
  }
};

module.exports.setAdmin = async (req, res, next) => {
  req.user.userType = 'admin';
  await req.user.save();
  res.status(200).send({ message: 'Ok!' });
};

module.exports.setClient = async (req, res, next) => {
  req.user.userType = 'client';
  await req.user.save();
  res.status(200).send({ message: 'Ok!' });
};

module.exports.updatePasswordUser = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;

  try {
    const isMatch = await bcrypt.compare(oldPassword, req.user.password);
    if (!isMatch)
      return res.status(400).send({ error: 'Old password is incorrect!' });
    req.user.password = newPassword;
    await req.user.save();
    res.status(200).send({ message: 'Update password successfully!' });
  } catch (e) {
    res.status(500).send();
  }
};

module.exports.login = async (req, res, next) => {
  try {
    const user = await User.findByCredentials(
      req.body.email,
      req.body.password
    );
    if (user.isActive == false){
      return res.status(401).json({message: "Not active account."});
    }
    const token = await user.generateAuthToken();
    res.status(200).send({ user, token });
  } catch (e) {
    res.json({ message: e.message, status: 400 });
  }
};

module.exports.logout = async (req, res, next) => {
  try {
    req.user.tokens = req.user.tokens.filter(
      (token) => token.token !== req.token
    );
    await req.user.save();
    res.send({ message: 'Logout successfully!' });
  } catch (e) {
    res.status(500).send();
  }
};

module.exports.logoutAll = async (req, res, next) => {
  try {
    req.user.tokens = [];
    await req.user.save();
    res.status(200).send({ message: 'Logout all device successfully!' });
  } catch (e) {
    res.status(500).send();
  }
};
/**
 * @todo api Avatar
 */
module.exports.uploadAvatar = async (req, res, next) => {
  try {
    console.log("Uploading avatar: ");
    req.user.avatar = req.file.url;
    await req.user.save(req.user);
    res.status(201).send({
      message: 'Upload message successfully',
      avatar: req.user.avatar,
    });
  } catch (e) {
    console.error(e);
    res.status(500).send(e);
  }
};

module.exports.deleteAvatar = async (req, res, next) => {
  try {
    req.user.avatar = undefined;
    await req.user.save();
    res.status(200).send({ message: 'Delete avatar successfully!' });
  } catch (e) {
    res.status(500).send();
  }
};

module.exports.getAvatarById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user || !user.avatar)
      throw new Error("User not found or User don't have avatar");
    res.status(200).send({
      avatar: user.avatar,
    });
  } catch (e) {
    res.status(404).send({ message: e.message });
  }
};

module.exports.getLikeTrips = async (req, res, next) => {
  try {
    const arrTrips = req.user.likeTrips;
    const result = await Promise.all(arrTrips.map(async (tripId) => {
      const trip = await Trip.findById(tripId).populate("fromStation toStation acrossStation coach").lean();
      return {...trip, image: trip.coach.image};
    }))
    return res.status(200).json(result)
  } catch (e) {
    res.status(404).send({ message: e.message });
  }
};

module.exports.toggleLikeTrip = async (req, res, next) => {
  try {
    const trip = await Trip.findById(req.params.tripId).lean();
    if(!trip) throw new Error('NOT_FOUND_TRIP');
    let action = 'Like'
    if(!req.user.likeTrips.includes(trip._id)){
      req.user.likeTrips.push(trip._id)
    }else {
      const index = req.user.likeTrips.indexOf(trip._id)
      req.user.likeTrips.splice(index, 1);
      action = 'Not like'
    }
    await req.user.save()
    res.status(200).send({ message: `${action} Trip successfully!` });
  } catch (e) {
    res.status(404).send({ message: e.message });
  }
};
