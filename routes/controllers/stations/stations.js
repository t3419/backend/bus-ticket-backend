const Station = require('../../../models/Station');
const Province = require('../../../models/Province')
const District = require('../../../models/District')

module.exports.createStation = async (req, res, next) => {
    try{
        const { province, district} = req.body;
        const exitProvince = await Province.findOne({name: province}).lean();
        const exitDistrict = await District.findOne({nameWithType: district}).lean();
        if(!exitProvince || !exitDistrict || exitDistrict.parentCode !== exitProvince.code) throw new Error("INVALID.PROVINCE.OR.DISTRICT")
        const newStation = new Station(req.body);
        newStation['image'] = 'http://oto-xemay.vn/Benxe.jpg';
        await newStation.save();
        res.status(201).json(newStation);
    } catch (error) {
        return res.status(500).json(error.message);
    }
};
/**
 * chưa fix được sort theo unicode
 */
module.exports.getStationPaginatedFilter = async (req, res, next) => {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const results = {};
    const sort = {};
    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':');
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
    }

    if (endIndex < (await Station.countDocuments().exec())) {
        results.next = {
            page: page + 1,
            limit: limit,
        };
    }

    if (startIndex > 0) {
        results.previous = {
            page: page - 1,
            limit: limit,
        };
    }
    let condition = req.query.province ? { province: req.query.province } : {};
    if(req.query.district) condition["district"] = req.query.district 
    try {
        results.results = await Station.find(condition)
            .sort(sort)
            .limit(limit)
            .skip(startIndex)
            .exec();
        res.status(200).json(results);
        next();
    } catch (err) {
        res.status(500).send();
    }
};

module.exports.getStationById = async (req, res, next) => {
    const { id } = req.params;
    try {
        const station = await Station.findById(id);
        if (!station) return res.status(404).send();
        res.status(200).json(station);
    } catch (error) {
        error.status(500).send();
    }
};

module.exports.updateStationById = async (req, res, next) => {
    try {
        console.log('aaaaaaa')
        const updates = Object.keys(req.body);
        const allowedUpdates = ['name', 'address', 'province', 'district', 'image'];
        const isValidOperation = updates.every((update) =>
            allowedUpdates.includes(update)
        );

        if (!isValidOperation)
            return res.status(400).send({ error: 'Invalid updates!' });
        const { province, district} = req.body;
        const exitProvince = await Province.findOne({name: province}).lean();
        const exitDistrict = await District.findOne({nameWithType: district}).lean();
        if(!exitProvince || !exitDistrict || exitDistrict.parentCode !== exitProvince.code) throw new Error("INVALID.PROVINCE.OR.DISTRICT")
        const station = await Station.findByIdAndUpdate(
            req.params.id,
            req.body,
            {
                new: true,
                runValidators: true,
            }
        );
        if (!station)
            return res.status(404).send({ error: 'Station not found!' });

        res.send(station);
    } catch (e) {
        res.status(500).send();
    }
};

module.exports.deleteStationById = async (req, res, next) => {
    try {
        const station = await Station.findByIdAndDelete(req.params.id);
        if (!station)
            return res.status(404).send({ error: 'Station not found!' });
        res.status(200).json({ message: 'Delete successfully!' });
    } catch (e) {
        res.status(500).send();
    }
};
