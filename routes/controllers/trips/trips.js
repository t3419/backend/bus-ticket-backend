const Trip = require("../../../models/Trip");
const Station = require("../../../models/Station");
const Coach = require('../../../models/Coach')
const { Seat } = require("../../../models/Seat");
const Comment = require('../../../models/Comment')
const Ticket = require('../../../models/Ticket') 
const Promise = require('bluebird')

const seat16 = [
  "A01","A02","A03","A04","A05","B01","B02","B03","B04","B05","C01","C02","C03","C04","C05","D05"
];

const seat29 = [
  "A01","A02","A03","A04","A05","A06","A07","B01","B02","B03","B04","B05","B06","B07","C01","C02","C03","C04","C05","C06", "C07","D01","D02","D03","D04","D05","D06", "D07", "E07"
]

const seat45 = [
  "A01","A02","A03","A04","A05","A06","A07","A08","A09","A10","A11","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","C01","C02","C03","C04","C05","C06", "C07","C08","C09","C10","C11","D01","D02","D03","D04","D05","D06","D07","D08","D09","D10","D11", "E11"
]
const seatCodes = new Array(seat16, seat29, seat45)

module.exports.createTrip = async (req, res, next) => {
  try {
    const { fromStation, toStation, acrossStation, startTime, price ,coachId, description = ''} = req.body;

    const coach = await Coach.findById(coachId).lean()
    if(!coach)  throw new Error("COACH_NOT_FOUND")

    if(!Array.isArray(acrossStation)) throw new Error("INVALID_TYPE_ARGUMENT")
    const array = new Array(fromStation,toStation)
    const checkStation = acrossStation.concat(array)
    const fromstation = await Station.countDocuments({_id: { $in: checkStation}})
    if (fromstation !== checkStation.length) throw new Error("STATION.NOT.EXIT")

    const a = (coach.numberSeat == 16) ? 0 : (coach.numberSeat == 29) ? 1 : 2

    const time = Array.isArray(startTime) ? startTime: new Array(startTime)

    const result = await Promise.all(time.map(async(time) => {
      const newTrip = new Trip({ fromStation, acrossStation, toStation,startTime:  time, price , coach: coach._id, description});
      seatCodes[a].forEach((code) => {
        const newSeat = new Seat({ code });
        newTrip.seats.push(newSeat);
      });
      const saveTrip = await newTrip.save()
      const trip = await Trip.findById(saveTrip._id).populate("fromStation toStation acrossStation coach").lean();
      return {...trip, image: trip.coach.image};
    }))

    return res.status(200).json(result)
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

module.exports.getTripsLimit = (req, res, next) => {
  const { limit } = req.params;
  Trip.find()
    .limit(parseInt(limit)).populate("fromStation toStation acrossStation coach").lean()
    .then((trip) => {
      const trips = trip.map((e) => {return { ...e, image: e.coach.image}})
      return res.status(200).json(trips)
    })
    .catch((err) => res.status(500).json(err));
};

module.exports.getTripsAll = (req, res, next) => {
  Trip.find().populate("fromStation toStation acrossStation coach").lean()
    .then((trip) => {
      const trips = trip.map((e) => {return { ...e, image: e.coach.image}})
      return res.status(200).json(trips)
    })
    .catch((err) => res.status(500).json(err));
};

module.exports.getTripById = (req, res, next) => {
  const { id } = req.params;
  console.log(id)
  Trip.findById(id).populate("fromStation toStation acrossStation coach").lean()
    .then((trip) => res.status(200).json({...trip, image: trip.coach.image}))
    .catch((err) => res.status(500).json(err));
};


module.exports.updateTripById = async (req, res, next) => {
  const { id } = req.params;
  const { fromStation, acrossStation, toStation, startTime, price } = req.body;
  console.log({ fromStation, acrossStation, toStation, startTime, price })
  console.log('aaaa',new Date(startTime))
  try {
    const query = Trip.findById(id);
    const trip = await query;
    if (!trip)
      return {
        status: 404,
        message: "Not found",
      };
    trip.fromStation = fromStation;
    trip.toStation = toStation;
    trip.startTime = new Date(startTime.toString());
    trip.acrossStation = acrossStation;
    trip.price = price;

    await trip.save();
    const rs = await query.populate("fromStation toStation acrossStation");
    return res.status(200).json(rs);
  } catch (error) {
    console.error(error);
    return res.status(500).json(error);
  }
};

module.exports.deleteTripById = (req, res, next) => {
  const { id } = req.params;
  Trip.deleteOne({ _id: id })
    .then((result) => {
      if (result.n === 0)
        return Promise.reject({
          status: 400,
          message: "Not found",
        });
      res.status(200).json({ message: "Delete successfully" });
    })
    .catch((err) => {
      if (err.status)
        return res.status(err.status).json({
          message: err.message,
        });
      return res.status(500).json(err);
    });
};

module.exports.searchTrips = async (req, res, next) => {
  try {
    const {fromProvince, toProvince, startTime, fromDistrict, toDistrict } = req.query
    console.log({fromProvince, toProvince, startTime, fromDistrict, toDistrict })
    const startOfDay = new Date(startTime)
    const endOfDay = new Date(new Date(startTime).getTime()+ 24 * 60 * 60 * 1000 -1)
    const timeCondition = startTime ? { startTime: { $gte: startOfDay , $lt: endOfDay} } : {}
    const trips = await Trip.find(timeCondition).populate('fromStation toStation acrossStation coach').lean();
    let tripReal = trips.map((trip) => {
      const from = trip.acrossStation.concat(new Array(trip.fromStation));
      const to = trip.acrossStation.concat(new Array(trip.toStation));
      let checkFrom = true;
      let checkTo = true;
      if(fromProvince){
        checkFrom = from.some((e) => { return fromDistrict?  e.province == fromProvince && e.district == fromDistrict : e.province == fromProvince })
      }
      if(toProvince){
        checkTo = to.some((e) => { return toDistrict?  e.province == toProvince && e.district == toDistrict : e.province == toProvince })
      }
      if(checkFrom && checkTo) return { ...trip, image: trip.coach.image}
      return null
    })
    tripReal = tripReal.filter((trip) => trip !== null)
    if (tripReal.length === 0)
      return res.status(404).send({ message: "Sorry! We don't have trip for you." });

    res.status(200).send(tripReal);
  } catch (e) {
    res.status(500).send(e);
  }
};

module.exports.getDataAnalyst = async (req, res) => {
  try {
    // const result = await Trip.aggregate([
    //   // {
    //   //   $match: {startTime !== null},
    //   // },
    //   // {
    //   //   $lookup: {
    //   //     from: "Ticket",
    //   //     localField: "_id",
    //   //     foreignField: "tripId",
    //   //     as: 'ticket'
    //   //   }
    //   // },
    //   // {
    //   //   $group: {
    //   //     _id:{ toStation: '$toStation', fromStation: '$fromStation'},
    //   //     // origId : { $addToSet: '$_id' },
    //   //     ticket : { $addToSet: '$ticket' },
    //   //   }
    //   // }
    //   // {
    //   //   $unwind: '$ticket'
    //   // },
    // ])

    // const a = result.filter((e) => e.ticket[0].length !== 0)
    // const a = result.filter((e) => e.ticket.length !== 0)
    // console.log(result.length)
    // console.log(a.length)

    // const trips = await Trip.find()
    //   .populate({
    //     path: 'coach',
    //     select: {
    //       'coachOwner': 1,
    //       'numberSeat': 1
    //     },
    //   })
    //   .populate({
    //     path: 'fromStation',
    //     select: {
    //       'name': 1,
    //     },
    //   })
    //   .populate({
    //     path: 'toStation',
    //     select: {
    //       'name': 1,
    //     },
    //   });
    // console.log(trips[0].startTime.toLocaleString().split(',')[0])
    // const arr = [];
    // trips.forEach((trip) => {
    //   if(arr.some((e) => e.coachOwner == trip.coach.coachOwner && e.fromStation == trip.fromStation.name  && e.toStation == trip.toStation.name && e.date == trip.startTime.toLocaleString().split(',')[0] && e.type == trip.coach.numberSeat)) {


    //   }
    // })

    let data =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    const { owner } = req.query;
    const coach = await Coach.find({ coachOwner: owner }).select('_id').lean();
    const arr = coach.map((coach) => coach._id)

    const now = new Date(new Date().setHours(0,0,0,0))
    const aMonthAgo = new Date(now.getTime() - 30 * 24 * 60 * 60 * 1000)

    const trips = await Trip.find({coach: { $in: arr}, startTime: {"$gte": aMonthAgo, "$lt": now}});

    console.log(trips.length)
    trips.forEach((trip) => {
      const time = trip.startTime.getTime() - aMonthAgo.getTime()
      if( time > 30 * 24 * 60 * 60 * 1000 || time < 0){
        return
      }
      const numberBookedSeat = trip.seats.reduce((total, seat) => {return seat.isBooked ? total+1 : total}, 0)
      console.log(`${trip._id}----${parseInt(time/(24 * 60 * 60 * 1000))} --- ${numberBookedSeat}`)
      data[parseInt(time/(24 * 60 * 60 * 1000))] += numberBookedSeat;
    })
    return res.status(200).json({coachOwner: owner, data,})
  } catch (e) {
    res.status(500).send(e);
  }
}

module.exports.getCommentTrip = async (req, res, next) => {
  try {
    const { id } = req.params;
    const trip = await Trip.findById(id).populate('coach').lean()
    if(!trip) throw new Error('TRIP_NOT_FOUND')
    const comment = await Comment.find({coachOwner: trip.coach.coachOwner})
      .populate({
        path: 'user',
        select: {
          "fullName": 1,
          "avatar": 1
        }
      })
    return res.status(200).json({comment: comment})

  } catch (error) {
    return res.status(500).json(error.message);
  }
};

module.exports.commentTrip = async (req, res, next) => {
  try {
    const { id } = req.params;
    const {comment, vote} = req.body
    if(!comment) throw new Error("INVALID_ARGUMENT")
    const trip = await Trip.findById(id).populate('coach').lean()
    console.log(trip)
    if(!trip) throw new Error('TRIP_NOT_FOUND')
    const checkTicket = await Ticket.countDocuments({tripId: trip._id, userId: req.user._id});
    if(trip.startTime.getTime() > new Date().getTime() || checkTicket ==0) throw new Error('INVALID_ACTION_COMMENT')
    await Comment.findOneAndUpdate(
      { 
        trip: trip._id,
        user: req.user._id
      },
      {
        coachOwner: trip.coach.coachOwner,
        comment,
        vote,
      },
      {upsert: true}
    )
    const result = await Comment.find({coachOwner: trip.coach.coachOwner})
      .populate({
        path: 'user',
        select: {
          "fullName": 1,
          "avatar": 1
        }
      })
    return res.status(200).json({comment: result})

  } catch (error) {
    return res.status(500).json(error.message);
  }
};
