const express = require("express");
const { authenticate, authorize } = require("../../../middlewares/auth");
const { uploadMultiImage } = require('../../../middlewares/uploadImage');
const coachController = require("./coach");


const router = express.Router();

router.get("/", authenticate, authorize('admin'), coachController.getCoach);
router.get("/paginated/", authenticate, authorize('admin'),coachController.getCoachPaginatedFilter )
router.get("/:id", authenticate, authorize('admin'), coachController.getCoachById);
router.post("/", authenticate, authorize('admin'), coachController.createCoach)
router.put("/:id", authenticate, authorize('admin'), coachController.updateCoachById);
router.post("/upload_image", authenticate, authorize('admin'), uploadMultiImage('images'), coachController.uploadImage);


module.exports = router;
