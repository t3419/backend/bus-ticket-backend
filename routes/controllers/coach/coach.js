const Coach = require("../../../models/Coach");

module.exports.getCoach = async (req, res, next) => {
    try {
        const coach = await Coach.find().sort('coachOwner').lean();
        return res.status(200).json({results: coach});
    } catch (e) {
      res.status(500).send();
    }
};

module.exports.getCoachById = async (req, res, next) => {
    try {
        const coach = await Coach.findById(req.params.id).lean();
        return res.status(200).json(coach);
    } catch (e) {
        res.status(500).send();
    }
};

module.exports.createCoach = async (req, res, next) => {
    try {
        let {name, coachOwner, numberSeat, description, image} = req.body;
        if(!image) image = ['https://cdnimg.vietnamplus.vn/t620/uploaded/natmjs/2014_10_02/giuong_nam.jpg']
        const check = await Coach.countDocuments({coachOwner,numberSeat }).lean();
        console.log(check)
        if(check > 0)  return res.status(200).json({message: "Xe đẫ tồn tại"});
        const coach = new Coach({
            name,
            coachOwner,
            numberSeat,
            description,
            image
        })
        const a = await coach.save()
        return res.status(200).json(a);
    } catch (e) {
        res.status(500).send();
    }
};

module.exports.getCoachPaginatedFilter = async (req, res, next) => {
    try {
        console.log('aaaaaa')
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
    
        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;
        const results = {};
        const totalCoach = await Coach.countDocuments().exec()
        results['totalCoach'] = totalCoach
        if (endIndex < totalCoach) {
            results.next = {
                page: page + 1,
                limit: limit,
            };
        }
    
        if (startIndex > 0) {
            results.previous = {
                page: page - 1,
                limit: limit,
            };
        }
        results.results = await Coach.find()
            .sort('coachOwner')
            .select('-image -description')
            .limit(limit)
            .skip(startIndex)
            .exec();
        return res.status(200).json(results);
    } catch (e) {
        res.status(500).send();
    }
};

module.exports.updateCoachById = async (req, res, next) => {
    try {
        console.log(req.body.description)
        if(!req.params.id || Array.isArray(req.body.image)) throw new Error('INVALID_ARGUMENT')
        const update = {}
        if(req.body.image) update['image'] = req.body.image;
        if(req.body.name) update['name'] = req.body.name;
        if(req.body.description) update['description'] = req.body.description;
        if(req.body.coachOwner) update['coachOwner'] = req.body.coachOwner;
        const coach = await Coach.findOneAndUpdate(
            {_id : req.params.id},
            update,
            {new: true}
        ).exec();
        return res.status(200).json(coach);
    } catch (e) {
        res.status(500).send();
    }
};

module.exports.uploadImage = async (req, res, next) => {
    try {
        console.log(req.files);
        const coach = await Coach.FindById(req.body.id);
        await coach.save();
        res.status(201).send({
          message: 'Upload image successfully',
          image: coach.image,
        });
    } catch (e) {
        res.status(500).send();
    }
};