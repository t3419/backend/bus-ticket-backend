const express = require('express');
const Province = require('../../../models/Province')
const District = require('../../../models/District')

const router = express.Router();

router.get('/', async(req, res) => {
    try {
        const province = await Province.find().select('-_id').sort('name');
        return res.status(200).json(province);
    } catch (error) {
        console.log(error)
    }
})

router.get('/:id', async(req, res) => {
    try {
        const { id } = req.params;
        const province = await Province.findOne({ code: id }).select('-_id');
        return res.status(200).json(province);
    } catch (error) {
        console.log(error)
    }
})

router.get('/:id/district', async(req, res) => {
    try {
        const { id } = req.params;
        const province = await Province.findOne({ code: id }).select('-_id').lean();
        const districtByProvince = await District.find({parentCode : id})
            .select('-_id name nameWithType pathWithType code parentCode')
            .sort('name')
            .lean()
        return res.status(200).json({
            ...province,
            district: districtByProvince,
        });
    } catch (error) {
        console.log(error)
    }
})

module.exports = router;