const express = require('express');
const stationRouter = require('./controllers/stations/index');
const tripRouter = require('./controllers/trips/index');
const userRouter = require('./controllers/users/index');
const ticketRouter = require('./controllers/tickets/index');
const provinceRouter = require('./controllers/province/index')
const coachRouter = require('./controllers/coach/index')
const router = express.Router();

router.use('/stations', stationRouter);
router.use('/trips', tripRouter);
router.use('/users', userRouter);
router.use('/tickets', ticketRouter);
router.use('/province', provinceRouter)
router.use('/district', provinceRouter)
router.use('/coach', coachRouter)

module.exports = router;
