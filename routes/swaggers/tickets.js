const pathOrderTickets = {
  post: {
    tags: ["Tickets"],
    summary: "Booking a new Ticket",
    parameters: [
      {
        name: "Ticket",
        in: "body",
        description: "Ticket with new values of properties",
        required: true,
        schema: {
          type: "object",
          properties: {
            tripId: {
              type: "string"
            },
            seatCodes: {
              type: "array",
              items: {
                type: "string"
              }
            }
          }
        }
      }
    ],
    responses: {
      "200": {
        description: "New Ticket created",
        schema: {
          type: "object",
          properties: {
            zalo_code: {
               type: "object"
            },
            ticketId: { type: "string" },
          },
        }
      },
      "405": { description: "Invalid input" }
    },
    security: [
      {
        token: []
      }
    ]
  }
};

const pathCreateTickets = {
  post: {
    tags: ["Tickets"],
    summary: "Confirm paid Ticket",
    parameters: [
      {
        name: "Ticket",
        in: "body",
        description: "Ticket with new values of properties",
        required: true,
        schema: {
          type: "object",
          properties: {
            ticketId: {
              type: "string"
            },
          }
        }
      }
    ],
    responses: {
      "200": {
        description: "Buy ticket successfully",
        schema: {
          $ref: "#/definitions/Ticket"
        }
      },
      "405": { description: "Invalid input" }
    },
    security: [
      {
        token: []
      }
    ]
  }
};

const definitionTicket = {
  type: "object",
  required: ["tripId", "userId", "seats", "totalPrice"],
  properties: {
    tripId: {
      type: "string"
    },
    userId: {
      type: "string"
    },
    seats: {
      $ref: "#/definitions/Seat"
    },
    totalPrice: {
      type: "integer"
    }
  }
};

module.exports = {
  pathOrderTickets,
  pathCreateTickets,
  definitionTicket
};
