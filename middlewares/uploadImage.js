const multer = require('multer');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require('multer-storage-cloudinary');

// ./uploads/avatar
// ./uploads/trip
// ./uploads/coach

cloudinary.config({
  cloud_name: 'dtnws8uep',
  api_key: '788583878192165',
  api_secret: 'yqScod8cqOtMYijLTQ6V1DN8TCw',
});

module.exports.uploadImage = (file) => {
  const upload = multer({
    storage: cloudinaryStorage({
      cloudinary: cloudinary,
      folder: 'letgo',
      // allowedFormats: ["jpg", "png"],
      // filename: function (req, file, cb) {
      //   cb(undefined, 'avatar');
      // },
    }),
  });
  return upload.single(file);
};

module.exports.uploadMultiImage = (files) => {
  const upload = multer({
    storage: cloudinaryStorage({
      cloudinary: cloudinary,
      folder: 'letgo',
      // allowedFormats: ["jpg", "png"],
      // filename: function (req, file, cb) {
      //   cb(undefined, 'avatar');
      // },
    }),
  });
  return upload.array(files, 10);
}