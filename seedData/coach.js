
const Coach = require('../models/Coach')

const coachData = [
    {
        "_id": "61a5d1a71f89744c785772b8",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Sao Việt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772b9",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Sao Việt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772ba",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Sao Việt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772bb",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Sao Việt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772bc",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Sao Việt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772bd",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Sao Việt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772be",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Sao Việt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772bf",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Sao Việt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772c0",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Sao Việt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772c2",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772c1",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Sao Việt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772c3",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772c4",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772c5",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772c8",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772c9",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772c6",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772ca",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772cc",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Văn Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772c7",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772ce",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Văn Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772cd",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Văn Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772cb",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Tiến Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772d1",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Văn Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772cf",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Văn Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772d2",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Văn Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772d3",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Văn Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772d4",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Văn Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772d0",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Văn Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772d5",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Văn Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772d6",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Quang Nghị",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772d8",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Quang Nghị",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772d7",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Quang Nghị",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772d9",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Quang Nghị",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772da",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Quang Nghị",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772db",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Quang Nghị",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772dc",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Quang Nghị",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772df",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Quang Nghị",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772e0",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Hưng Long",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772dd",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Quang Nghị",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772de",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Quang Nghị",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772e1",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Hưng Long",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772e3",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Hưng Long",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772e4",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Hưng Long",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772e2",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Hưng Long",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772e5",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Hưng Long",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772e6",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Hưng Long",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772e8",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Hưng Long",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772e9",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Hưng Long",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772eb",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Xuân Trường",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772e7",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Hưng Long",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772ec",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Xuân Trường",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772ed",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Xuân Trường",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772ea",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Xuân Trường",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772ef",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Xuân Trường",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772f0",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Xuân Trường",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772ee",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Xuân Trường",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772f1",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Xuân Trường",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772f3",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Xuân Trường",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772f4",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772f5",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772f6",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772f2",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Xuân Trường",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772f8",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772f7",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772f9",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772fb",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772fc",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c785772fd",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577305",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577303",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577306",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577302",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c785772ff",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577300",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577308",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772fe",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577304",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577309",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c785772fa",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Quốc Đạt",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577301",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857730a",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577307",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Xuân Tráng Limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857730b",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857730c",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857730d",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857730e",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857730f",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577310",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577311",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Thanh Bình Xanh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577313",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577312",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577314",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577315",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577316",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577317",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577318",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577319",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857731b",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857731a",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Nam Á Châu",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857731c",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Trọng Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857731f",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Trọng Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577321",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Trọng Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857731e",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Trọng Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857731d",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Trọng Minh",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577320",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Trọng Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577322",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Trọng Minh",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577323",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Trọng Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577325",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Trọng Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577324",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Trọng Minh",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577326",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577327",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577328",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857732a",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577329",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857732c",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857732d",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857732e",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857732f",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577330",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Khanh Phong",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577331",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Khanh Phong",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857732b",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Thiện Thành limousine",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577332",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Khanh Phong",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577334",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Khanh Phong",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577336",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Khanh Phong",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577333",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Khanh Phong",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577337",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Khanh Phong",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577338",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Khanh Phong",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857733b",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Minh Quốc",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577339",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Khanh Phong",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857733a",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Minh Quốc",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577335",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Khanh Phong",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857733c",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Minh Quốc",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857733d",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Minh Quốc",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857733e",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Minh Quốc",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577340",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Minh Quốc",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577341",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Minh Quốc",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577342",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Minh Quốc",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577343",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Minh Quốc",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577344",
        "image": [
            "https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Toyota Hiace",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c7857733f",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Minh Quốc",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577345",
        "image": [
            "https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Mercedes Sprinter",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577346",
        "image": [
            "https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Solati",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 16
    },
    {
        "_id": "61a5d1a71f89744c78577348",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Samco Felix",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c78577347",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai County",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857734a",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Meadow TB85s",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 29
    },
    {
        "_id": "61a5d1a71f89744c7857734b",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Universe",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857734c",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Hyundai Aero Space",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c7857734d",
        "image": [
            "https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Universe",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 45
    },
    {
        "_id": "61a5d1a71f89744c78577349",
        "image": [
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg",
            "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"
        ],
        "description": "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin",
        "name": "Thaco Garden TB79s",
        "coachOwner": "Thuận Tiến",
        "numberSeat": 29
    }
]


const generateCoach = async() => {
    try {
        let count = 0;
        coachData.forEach(async (coach) => {
            count ++;
            const exitData = await Coach.findById(coach._id).lean();
            if(!exitData) {
                const newCoach = new Coach({
                    _id: coach._id,
                    name: coach.name,
                    image: coach.image,
                    coachOwner: coach.coachOwner,
                    numberSeat: coach.numberSeat,
                    description: coach.description,
                })
                await newCoach.save()
            }
        })
        console.log(`Seed ${count} Coach Success`);
    } catch (error) {
        throw new Error(error.message);
    }
};
module.exports = {
    generateCoach,
    coachData
}