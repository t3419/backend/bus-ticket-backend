const User = require('../models/User')

const randomDate = (start, end) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

const fullname = ['Nguyen Tien Dat', "Pham Trong Dai", "Tran Quoc Cuong", 'Do Quang Anh', "Dinh Viet Anh"]

const generateUser = async() => {
    try {
        const arr =  Array.from(Array(10).keys())
        let count = 0;
        arr.forEach(async (index) => {
            const newUser = new User({
                fullName: fullname[index % fullname.length],
                email: `client${index +1}@gmail.com`,
                password: '123123a',
                dayOfBirth: randomDate(new Date(20000, 0, 1), new Date(2000,11,11)).toString(),
                phoneNumber: '0969948564',
                isActive: true,
                confirmCode: 123456,
                dateConfirmCode: new Date()
            })
            count ++;
            await newUser.save() 
            const token = await newUser.generateAuthToken();
        })

        console.log(`Seed ${count} User Success`);
    } catch (error) {
        throw new Error(error.message);
    }

};

module.exports = {
    generateUser
}