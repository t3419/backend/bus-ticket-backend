const {generateProvince} = require('./province')
const {generateDistrict} = require('./district')
const {generateStation} = require('./station')
const {generateCoach} = require('./coach')
const { generateUser } = require('./user')
const { generateTrip } = require('./trip')

const SEED_DATA = true

const seed = async() => {
	try {
        if (SEED_DATA)  await _seed()
    } catch (error) {
        throw new Error(error.message);
    }
};

const _seed = async () => {
	// await generateProvince();
	// await generateDistrict();
    // await generateStation();
    // await generateCoach();
   // generateUser()
 // generateTrip()
   
};
module.exports = {
    seed,
}
