const Station = require('../models/Station')
const Province = require('../models/Province')
const District = require('../models/District')
const Promise = require("bluebird");
const fs = require('fs')

const data = [
    {
        "_id": "619f58f3e763b3374c7a7933",
        "name": "Bến Xe Giáp Bát",
        "address": "Km6 đường Giải Phóng, quận Hoàng Mai, thành phố Hà Nội",
        "province": "Hà Nội",
        "district": "Quận Hoàng Mai",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402285518_2.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7937",
        "name": "Bến Xe Miền Đông",
        "address": "Số 292 Đinh Bộ Lĩnh, Phường 26, Q.Bình Thạnh, TP.Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận Bình Thạnh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402286125_1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793c",
        "name": "Bến Xe Lục Ngạn",
        "address": "Xã Nghĩa Hồ, Huyện Lục Ngạn, Tỉnh Bắc Giang",
        "province": "Bắc Giang",
        "district": "Thành phố Bắc Giang",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408433465_3-bac_x20_giang-ben_x20_xe_x20_luc_x20_ngan.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793b",
        "name": "Bến Xe Nước Mát",
        "address": "Thôn Nước Mát, Xã Âu Lâu, TP Yên Bái",
        "province": "Yên Bái",
        "district": "Thành phố Yên Bái",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408005755_2-ben_x20_xe_x20_khach_x20_nuoc_x20_mat_x20_yen_x20_bai.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7939",
        "name": "Bến Xe Trung Tâm Đà Nẵng",
        "address": "Số 201 Tôn Đức Thắng, P Hòa Minh, Q Liên Chiểu, TP Đà Nẵng",
        "province": "Đà Nẵng",
        "district": "Quận Liên Chiểu",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402287484_6.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7935",
        "name": "Bến Xe Nước Ngầm",
        "address": "Số 01 Ngọc Hồi, Hoàng Liệt, Q Hoàng Mai, TP Hà Nội",
        "province": "Hà Nội",
        "district": "Quận Hoàng Mai",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1481339391_ben_xe_nuoc_ngam_1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7934",
        "name": "Bến Xe Lương Yên",
        "address": "Số 03 Nguyễn Khoái, Bạch Đằng, Q Hai Bà Trưng, TP Hà Nội",
        "province": "Hà Nội",
        "district": "Quận Hai Bà Trưng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7936",
        "name": "Bến Xe Gia Lâm",
        "address": "Số 9 Ngô Gia Khảm, Q.Long Biên, TP.Hà Nội",
        "province": "Hà Nội",
        "district": "Quận Long Biên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402285170_1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793a",
        "name": "Bến Xe Đức Linh",
        "address": "Huyện Đức Linh, Tỉnh Bình Thuận",
        "province": "Bình Thuận",
        "district": "Huyện Đức Linh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408503311_1-binh_x20_thuan-ben_x20_xe_x20_duc_x20_linh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7938",
        "name": "Bến Xe Miền Tây",
        "address": "Số 395 Kinh Dương Vương, P.An Lạc, Q.Bình Tân, TP.Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 3",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402286273_2.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7940",
        "name": "Bến Xe Nam Phước",
        "address": "QL1, Thị Trấn Nam Phước, Huyện Duy Xuyên, Tỉnh Quảng Nam",
        "province": "Quảng Nam",
        "district": "Huyện Duy Xuyên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405157002_1-quang-nam-ben-xe-nam-phuoc.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7942",
        "name": "Bến xe Bình Thuận",
        "address": "Từ Văn Tư - Phú Trinh - TX. Phan Thiết - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thành phố Phan Thiết",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408965144_1-binh_x20_thuan-ben_x20_xe_x20_binh_x20_thuan.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793d",
        "name": "Bến Xe Liên Tỉnh Đà Lạt",
        "address": "Số 01 Tô Hiến Thành, Phường 3, TP.Đà Lạt, Tỉnh Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Thành phố Đà Lạt",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405138038_1-da_x20_lat-ben-xe-lien-tinh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793e",
        "name": "Bến Xe Bãi Cháy",
        "address": "Khu 1, Phường Giếng Đáy, TP Hạ Long, Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Hạ Long",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408520399_2.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a793f",
        "name": "Bến Xe Bến Tre",
        "address": "Số 303 Đoàn Hoàng Minh, Phú Khương, TP Bến Tre, Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405141325_1-ben-tre-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7941",
        "name": "Bến xe Gành Hào",
        "address": "Phan Ngọc Hiển - Gành Hào - Đông Hải - Bạc Liêu",
        "province": "Bạc Liêu",
        "district": "Thành phố Bạc Liêu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7944",
        "name": "Bến xe Ngã Tư Vũng Tàu",
        "address": "Quốc lộ 1A - An Bình - TP. Biên Hòa - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Thành phố Biên Hòa",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408965312_1-vung_x20_tau-ben_x20_xe_x20_nga_x20_tu.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7943",
        "name": "Bến xe Chùa Non Nước",
        "address": "Đông Hải - Hoà Hải - Quận Ngũ Hành Sơn - Đà Nẵng",
        "province": "Đà Nẵng",
        "district": "Quận Ngũ Hành Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7946",
        "name": "Bến xe Cẩm Xuyên",
        "address": "Huyện Cẩm Xuyên - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7945",
        "name": "Bến xe Hồng Lĩnh",
        "address": "Thị xã Hồng Lĩnh - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7947",
        "name": "Bến Xe Khách Nghi Sơn",
        "address": "Xã Nghi sơn - Huyện Tĩnh Gia - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794a",
        "name": "Bến Xe Tam Quan",
        "address": "Thị trấn Tam Quan, Huyện Hoài Nhơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Thị xã Hoài Nhơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7948",
        "name": "Bến xe Quỹ Nhất",
        "address": "Xá Nghĩa Hòa, Huyện Nghĩa Hưng, Tỉnh Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7949",
        "name": "Bến Xe Chi Lăng",
        "address": "Thị Trấn Chi Lăng, Huyện Tịnh Biên, Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Tịnh Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794d",
        "name": "Bến xe Đô Lương",
        "address": "Xã Văn Sơn - Huyện Đô Lương - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Đô Lương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794b",
        "name": "Bến xe Lệ Thủy",
        "address": "Cam Thủy - Lệ Thủy - Quảng Bình",
        "province": "Quảng Bình",
        "district": "Huyện Lệ Thủy",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794c",
        "name": "Bến xe Bắc Hà",
        "address": "Thị trấn Bắc Hà - H. Bắc Hà - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794e",
        "name": "Bến xe Vĩnh Bảo",
        "address": "Xã Nhân Hòa - Huyện Vĩnh Bảo - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Huyện Vĩnh Bảo",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1411789591_10.1._x20_ben_x20_xe_x20_vinh_x20_bao.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7951",
        "name": "Bến xe Bến Trại",
        "address": "Xã Tiền Phong - huyện Thanh Miện - Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a794f",
        "name": "Bến xe khách Vĩnh Thuận",
        "address": "Ấp Vĩnh Phước II, thị trấn Vĩnh Thuận, huyện Vĩnh Thuận, tỉnh Kiên Giang",
        "province": "Kiên Giang",
        "district": "Huyện Vĩnh Thuận",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7950",
        "name": "Bến xe Chiêm Hóa",
        "address": "Huyện Chiêm Hóa, Tỉnh Tuyên Quang",
        "province": "Tuyên Quang",
        "district": "Thành phố Tuyên Quang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7952",
        "name": "Bến xe Phía Bắc Buôn Ma Thuột",
        "address": "Số 71 Nguyễn Chí Thanh - P. Tân An - TP. Buôn Mê Thuột - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Lắk",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408503697_2-bmt-ben_x20_xe_x20_phia_x20_bac.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7956",
        "name": "Bến xe Đức Huệ",
        "address": "Kv 1, thị trấn Đông Thành, Huyện Đức Huệ, Long An",
        "province": "Long An",
        "district": "Huyện Đức Huệ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7953",
        "name": "Bến xe Ea H'leo - Đắk Lắk",
        "address": "Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Lắk",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7954",
        "name": "Bến Xe Cẩm Yên",
        "address": "Cẩm Yên - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7955",
        "name": "Bến Xe Đò Quan",
        "address": "Tỉnh lộ 490C, Phường Cửa Nam, TP Nam Định, Tỉnh Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7957",
        "name": "Bến Xe Thành Công",
        "address": "Số 604 Phú Riềng Đỏ, Tân Xuân, Đồng Xoài, Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Phú Riềng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795a",
        "name": "Bến xe La Tiến",
        "address": "Nguyên Hoà - Phù Cừ - Hưng Yên",
        "province": "Hưng Yên",
        "district": "Thành phố Hưng Yên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7958",
        "name": "Bến xe Bắc Ruộng",
        "address": "Xã Bắc Ruộng, Huyện Tánh Linh, Tỉnh Bình Thuận",
        "province": "Bình Thuận",
        "district": "Huyện Tánh Linh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408502626_1-binh_x20_thuan-ben_x20_xe_x20_bac_x20_ruong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7959",
        "name": "Bến xe Sông Hinh",
        "address": "Khu phố 4 - thị trấn Hai Riêng - huyện Sông Hinh - Phú Yên",
        "province": "Phú Yên",
        "district": "Huyện Sông Hinh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795b",
        "name": "Bến xe Trực Ninh",
        "address": "Cụm Công nghiệp thị trấn Cổ Lễ - huyện Trực Ninh – tỉnh Nam Định;",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795c",
        "name": "Bến xe Ninh Sơn",
        "address": "Khu phố 8, thị trấn Tân Sơn, huyện Ninh Sơn, tỉnh Ninh Thuận",
        "province": "Ninh Thuận",
        "district": "Huyện Ninh Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795d",
        "name": "Bến Xe Trường Hải",
        "address": "Đường Phú Riềng Đỏ, Phường Tân Xuân, Thị Xã Đồng Xoài, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Phú Riềng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795e",
        "name": "Bến Xe Cầu Gồ",
        "address": "Thị trấn Cầu Gồ, Yên Thế, Bắc Giang",
        "province": "Bắc Giang",
        "district": "Thành phố Bắc Giang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a795f",
        "name": "Bến xe Diên Khánh",
        "address": "Quốc lộ 1 - Diên Khánh - Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Huyện Diên Khánh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408965499_2-khanh_x20_hoa-ben_x20_xe_x20_dien_x20_khanh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7962",
        "name": "Bến xe Quảng Trị",
        "address": "Đường Trần Hưng Đạo - Phường 2 - TP. Đông Hà - Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thành phố Đông Hà",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7960",
        "name": "Bến xe Hoàn Lão",
        "address": "Tiểu khu 1 - Hoàn Lão - H. Bố Trạch - Quảng Bình",
        "province": "Quảng Bình",
        "district": "Huyện Bố Trạch",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7961",
        "name": "Bến xe Quế Sơn",
        "address": "Tổ 5 - Thị trấn Đông Phú - H. Quế Sơn - Quảng Nam",
        "province": "Quảng Nam",
        "district": "Huyện Quế Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7966",
        "name": "Bến xe Nghĩa Lộ",
        "address": "Thị xã Nghĩa Lộ - Yên Bái",
        "province": "Yên Bái",
        "district": "Thành phố Yên Bái",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7963",
        "name": "Bến xe Hòa Thành",
        "address": "Khu phố 2 - Thị trấn Hòa Thành - H. Hòa Thành - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7964",
        "name": "Bến xe Sơn Dương",
        "address": "Tân Phú, thị trấn Sơn Dương, Tuyên Quang",
        "province": "Tuyên Quang",
        "district": "Thành phố Tuyên Quang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7967",
        "name": "Bến Xe Hiệp Hòa",
        "address": "Khu 4, Thị trấn Thắng, Huyện Hiệp Hòa, Tỉnh Bắc Giang",
        "province": "Bắc Giang",
        "district": "Thành phố Bắc Giang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7965",
        "name": "Bến xe Mậu A",
        "address": "Khu phố 1 - Thị trấn Mậu A - H. Văn Yên - Yên Bái",
        "province": "Yên Bái",
        "district": "Thành phố Yên Bái",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7969",
        "name": "Bến xe phía bắc Lạng Sơn",
        "address": "Quốc lộ 1 A - Cao Lộc - Lạng Sơn",
        "province": "Lạng Sơn",
        "district": "Thành phố Lạng Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7968",
        "name": "Bến xe Liên Hương",
        "address": "Nguyễn Thị Minh Khai - Thị trấn Liên Hương - Tuy Phong - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Huyện Tuy Phong",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796b",
        "name": "Bến Xe Bình Đại",
        "address": "Ấp 3 Bình Thới, Thị trấn Bình Đại, Huyện Bình Đại, Tỉnh Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796a",
        "name": "Bến xe Hậu Nghĩa",
        "address": "Hậu Nghĩa, Đức Hòa, Long An",
        "province": "Long An",
        "district": "Huyện Đức Hòa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796c",
        "name": "Bến xe Cẩm Khê",
        "address": "Khu 4 TT Sông Thao - Cẩm Khê - Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796d",
        "name": "Bến xe Sầm Sơn",
        "address": "Sầm Sơn, Tỉnh Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7970",
        "name": "Bến Xe Khánh Bình",
        "address": "Xã Khánh Bình - Huyện An Phú - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện An Phú",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796f",
        "name": "Bến xe Nghĩa Hưng",
        "address": "Thị trấn Liễu Đề - H. Nghĩa Hưng - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a796e",
        "name": "Bến xe Kiên Giang",
        "address": "Ấp So Đũa - Xã Vĩnh Hòa Hiệp - Huyện Châu Thành - Tỉnh Kiên Giang",
        "province": "Kiên Giang",
        "district": "Huyện Châu Thành",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7971",
        "name": "Bến xe Dầu Giây",
        "address": "QL1A, Thống Nhất, Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Thống Nhất",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7974",
        "name": "Bến Xe Thị Trấn Óc Eo",
        "address": "Tỉnh lộ 943 - Thị trấn Óc Eo - Huyện Thoại Sơn - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Thoại Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7977",
        "name": "Bến xe Long Khánh",
        "address": "Đường Hùng Vương - Phường Xuân Trung - TX. Long Khánh - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Thành phố Long Khánh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408512270_1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7978",
        "name": "Bến xe phía bắc Nha Trang",
        "address": "Số 01 đường 2/4 - Vĩnh Hòa - Nha Trang - Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Thành phố Nha Trang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7973",
        "name": "Bến xe Cồn Thoi",
        "address": "Xã Cồn Thoi, Huyện Kim Sơn, Ninh Bình",
        "province": "Ninh Bình",
        "district": "Thành phố Ninh Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7972",
        "name": "Bến xe Sao Đỏ",
        "address": "Sao Đỏ, Thị Xã Chí Linh,Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7975",
        "name": "Bến Xe Tịnh Biên",
        "address": "Thị trấn Chi Lăng - Huyện Tịnh Biên - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Tịnh Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7976",
        "name": "Bến Xe Bình Khánh",
        "address": "Đường Hàm Nghi - Phường Bình Khánh - Thành Phố Long Xuyên, Tỉnh An Giang",
        "province": "An Giang",
        "district": "Thành phố Long Xuyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7979",
        "name": "Bến xe Bồng Tiên",
        "address": "Vũ Tiến, Vũ Thư, Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797a",
        "name": "Bến xe Chợ Lục",
        "address": "Thái Xuyên, Thái Thụy, Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797b",
        "name": "Bến xe Hưng Hà",
        "address": "Huyện Hưng Hà, Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797c",
        "name": "Bến xe Phú Bình",
        "address": "Xã Bình Yên, Huyện Định Hóa, Tỉnh Thái Nguyên",
        "province": "Thái Nguyên",
        "district": "Thành phố Thái Nguyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797d",
        "name": "Bến Xe phía Tây Thanh Hóa",
        "address": "Nguyễn Trãi, P.Phú Sơn, TP. Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7980",
        "name": "Bến Xe Lâm Hà",
        "address": "Xã Tân Hà - Lâm Hà - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Huyện Lâm Hà",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797f",
        "name": "Bến Xe Long Bình",
        "address": "Thị trấn Long Bình, Huyện An Phú, Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện An Phú",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a797e",
        "name": "Bến xe Hoài Nhơn",
        "address": "Huyện Hoài Nhơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Thị xã Hoài Nhơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7981",
        "name": "Bến xe Sơn Động",
        "address": "Xã An Lập-Huyện Sơn Động-Tỉnh Bắc Giang",
        "province": "Bắc Giang",
        "district": "Thành phố Bắc Giang",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1426325665_w1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7982",
        "name": "Bến Xe Uông Bí",
        "address": "Khu phố 3, phường Yên Thanh - Uông Bí - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Uông Bí",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7983",
        "name": "Bến Xe Khách Hòn Gai",
        "address": "Lê Thánh Tôn, Hồng Gai - Hạ Long - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Hạ Long",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7984",
        "name": "Bến xe Yaun Pa",
        "address": "Số 313 Trần Hưng Đạo, P. Hòa Bình, TX. Ayun Pa, Tỉnh Gia Lai",
        "province": "Gia Lai",
        "district": "Thị xã Ayun Pa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7986",
        "name": "Bến Xe Phố Mới (Lào Cai)",
        "address": "Đường Minh Khai, Phường Phố Mới - Lào Cai - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7987",
        "name": "Bến xe Đức Phổ",
        "address": "Khối 5, thị trấn Đức Phổ, huyện Đức Phổ, tỉnh Quảng Ngãi",
        "province": "Quảng Ngãi",
        "district": "Thành phố Quảng Ngãi",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7988",
        "name": "Bến xe huyện Sơn Hòa",
        "address": "Khu phố Trung Hòa - thị trấn Củng Sơn - huyện Sơn Hòa - Phú Yên",
        "province": "Phú Yên",
        "district": "Huyện Sơn Hòa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7985",
        "name": "Bến xe khách Đà Nẵng",
        "address": "Phường Hòa An, Cẩm Lệ, Thành phố Đà Nẵng",
        "province": "Đà Nẵng",
        "district": "Quận Cẩm Lệ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7989",
        "name": "Bến xe Hải Hà",
        "address": "Thị trấn Hải Hà - huyện Hải Hà - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Huyện Hải Hà",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798a",
        "name": "Bến xe Nam Sách",
        "address": "xã An Lâm - huyện Nam Sách - tỉnh Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798b",
        "name": "Bến xe Trung Hà",
        "address": "Quốc lộ 32, bến xe Trung Hà - Ba Vì - Hà Nội",
        "province": "Hà Nội",
        "district": "Huyện Ba Vì",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798c",
        "name": "Bến xe Phía Nam TP Nam Định",
        "address": "Bến xe Phía Nam Thành Phố Nam Định - Nam Định - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798f",
        "name": "Bến xe Quỳ Hợp",
        "address": "Km11+200 khối 2, thị trấn Quỳ Hợp - Quỳ Hợp - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Quỳ Hợp",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798d",
        "name": "Bến xe Tiên Kỳ",
        "address": "Xã Tiên Kỳ - Tân Kỳ - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Tân Kỳ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a798e",
        "name": "Bến xe Hoàng Mai",
        "address": "Km383+800 QL1A, thị trấn Hoàng mai - Quỳnh Lưu - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Quỳnh Lưu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7990",
        "name": "Bến Xe Liên Tỉnh Quảng Trị",
        "address": "Phường 1, Thị xã Quảng Trị - Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thị xã Quảng Trị",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7991",
        "name": "Bến xe Cửa Lò",
        "address": "Xã Nghi Thu - Cửa Lò - Nghệ An",
        "province": "Nghệ An",
        "district": "Thị xã Cửa Lò",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7992",
        "name": "Bến Xe Khách Bình An",
        "address": "Phường Tân Hoà, Thành Phố Hòa Bình, Tỉnh Hoà Bình",
        "province": "Hoà Bình",
        "district": "Thành phố Hòa Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7993",
        "name": "Bến xe Quỳnh Nhai",
        "address": "hị trấn Phiêng Lanh - Quỳnh Nhai - Sơn La",
        "province": "Sơn La",
        "district": "Huyện Quỳnh Nhai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7994",
        "name": "Bến xe Phương Trang",
        "address": "Đà Lạt - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Thành phố Đà Lạt",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7997",
        "name": "Bến xe Tà Lùng",
        "address": "Phục Hòa - Cao Bằng",
        "province": "Cao Bằng",
        "district": "Thành phố Cao Bằng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799a",
        "name": "Bến xe khách Thượng Lý",
        "address": "Số 52 đường Hà Nội - Phường Sở Dầu - Hồng Bàng - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Quận Hồng Bàng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799b",
        "name": "Bến xe Con Cuông",
        "address": "Thị Trấn Con Cuông - Con Cuông - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Con Cuông",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799d",
        "name": "Đối diện bến xe Sapa",
        "address": "Điện Biên Phủ - TT Sapa - Lào Cai",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7998",
        "name": "Bến Xe Chơn Thành",
        "address": "Khu phố 2, Thị trấn Chơn Thành, Huyện Chơn Thành, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Chơn Thành",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7995",
        "name": "Bến xe Yên Lạc",
        "address": "Thị Trấn Yên Lạc - Yên Lạc - Vĩnh Phúc",
        "province": "Vĩnh Phúc",
        "district": "Huyện Yên Lạc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799e",
        "name": "Bến Xe Phố Lu",
        "address": "Thị trấn Phố Lu - Bảo Thắng - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a0",
        "name": "Bến xe Sô Tô",
        "address": "Quảng Xương - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799f",
        "name": "Bến Xe Yên Lập",
        "address": "Huyện Yên Lập, Tỉnh Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a1",
        "name": "Bến xe Kinh Môn",
        "address": "Thị trấn Kinh Môn, Huyện Kinh môn, Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a799c",
        "name": "Đối diện bến xe Sapa",
        "address": "Điện Biên Phủ - TT Sapa - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7996",
        "name": "Bến Xe Sơn Tây",
        "address": "Quốc lộ 21, Thị xã Sơn Tây, TP Hà Nội",
        "province": "Hà Nội",
        "district": "Thị xã Sơn Tây",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7999",
        "name": "Bến xe phía Đông Sao Đỏ",
        "address": "Cụm công nghiệp Hoàng Tân, phường Hoàng Tân - Chí Linh - Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a7",
        "name": "Bến xe Bảo Bình",
        "address": "Xã Bảo Bình - Huyện Cẩm Mỹ - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Cẩm Mỹ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a8",
        "name": "Bến xe Hộ Phòng",
        "address": "Quốc lộ 1A - Hộ Phòng - Giá Rai - Bạc Liêu",
        "province": "Bạc Liêu",
        "district": "Thành phố Bạc Liêu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ab",
        "name": "Bến Xe Tây Sơn - Hà Tĩnh",
        "address": "Km84+450 QL8, Thị trấn Tây Sơn - Hương Sơn - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a2",
        "name": "Bến xe Ninh Giang",
        "address": "Thị trấn Ninh Giang - huyện Ninh Giang - tỉnh Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a3",
        "name": "Bến xe Hải Hậu",
        "address": "Thị trấn Yên Định, Hải Hậu, Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a4",
        "name": "Bến xe Tân Biên",
        "address": "Khóm 2 - Thị trấn Tân Biên - Tân Biên - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a5",
        "name": "Bến xe Vân Đồn",
        "address": "Khu 7, Thị trấn Cái Rồng - H. Vân Đồn - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Huyện Vân Đồn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a6",
        "name": "Bến xe Tân Hồng",
        "address": "Quốc lộ 30 - Sa Rài - Tân Hồng - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Huyện Tân Hồng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79a9",
        "name": "Bến xe Quất Lâm",
        "address": "Tổ dân phố Lâm Khang, Thị trấn Quất Lâm - Giao Thủy - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79aa",
        "name": "Bến Xe Thanh Sơn",
        "address": "Quốc lộ 32 - Thị Trấn Thanh Sơn - Thanh Sơn - Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ac",
        "name": "Bến xe Phước Long (Bạc Liêu)",
        "address": "Tỉnh lộ 2 - Thị trấn Phước Long - Phước Long - Bạc Liêu",
        "province": "Bạc Liêu",
        "district": "Thành phố Bạc Liêu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ae",
        "name": "Bến xe Bình Phước",
        "address": "Số 741 - Tân Xuân - TX Đồng Xoài - Bình Phước",
        "province": "Bình Phước",
        "district": "Thành phố Đồng Xoài",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ad",
        "name": "Bến xe huyện Ba Bể",
        "address": "Quốc lộ 279 - Thị trấn Chợ Rã - Ba Bể - Bắc Kạn",
        "province": "Bắc Kạn",
        "district": "Thành Phố Bắc Kạn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79af",
        "name": "Bến xe Phan Rí Cửa",
        "address": "Số 2 Lý Thường Kiệt - Phan Rí Cửa - Tuy Phong - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Huyện Tuy Phong",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b0",
        "name": "Bến xe Tánh Linh",
        "address": "Quốc lộ 55 - Tánh Linh - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Huyện Tánh Linh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b2",
        "name": "Bến xe Thị Xã La Gi",
        "address": "Số 206 Thống Nhất - Tân Thiện - Thị xã La Gi - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thị xã La Gi",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b1",
        "name": "Bến xe Cái Nước",
        "address": "Phường 8 - Thị xã Cà Mau - Tỉnh Cà Mau ‎",
        "province": "Cà Mau",
        "district": "Thành phố Cà Mau",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b3",
        "name": "Bến xe Mường Chà",
        "address": "Mường Chà - Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b4",
        "name": "Bến xe Bản Phủ",
        "address": "Bản Phủ - Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b5",
        "name": "Bến xe Thị Xã Mường Lay",
        "address": "Quốc lộ 12 - Na Lay - Mường Lay - Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b6",
        "name": "Bến xe Ghi Sê",
        "address": "Phường 2 - TX Sa Đéc - Tân Phú Đông - Sa Đéc - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Thành phố Sa Đéc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b7",
        "name": "Bến xe Sa Đéc",
        "address": "Nguyễn Sinh Sắc - Sa Đéc - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Thành phố Sa Đéc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b8",
        "name": "Bến xe Châu Thành",
        "address": "Số Số 161 Quốc lộ 80 - Cái Tàu Hạ - Châu Thành - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Huyện Châu Thành",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ba",
        "name": "Bến xe Vĩnh Trụ",
        "address": "Thị trấn Vĩnh Trụ - Lý Nhân - Hà Nam",
        "province": "Hà Nam",
        "district": "Huyện Lý Nhân",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79bc",
        "name": "Bến xe Hương Khê",
        "address": "Thị trấn Hương Khê - Hương Khê - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79b9",
        "name": "Bến xe Yên Minh",
        "address": "Số 119 Trần Hưng Đạo - Thị trấn Yên Minh - Yên Minh - Hà Giang",
        "province": "Hà Giang",
        "district": "Thành phố Hà Giang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79bb",
        "name": "Bến Xe Đan Phượng",
        "address": "Số 252 Tây Sơn, Thị trấn Phùng, Huyện Đan Phượng, Hà Nội",
        "province": "Hà Nội",
        "district": "Huyện Đan Phượng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79bd",
        "name": "Bến xe Đức Thọ",
        "address": "Quốc lộ 8A - Đức Yên - Đức Thọ - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79be",
        "name": "Bến xe Kỳ Anh",
        "address": "Quốc lộ 1A - Thị trấn Kỳ Anh - Kỳ Anh - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79bf",
        "name": "Bến xe Hải Tân",
        "address": "Đường Lê Thanh Nghị, phường Hải Tân, TP Hải Dương, Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c0",
        "name": "Bến xe Lạc Long",
        "address": "Số 20 Cù Chính Lan - Minh Khai - Hồng Bàng - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Quận Hồng Bàng",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408964722_2-hai_x20_phong-ben_x20_xe_x20_lac_x20_long.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c1",
        "name": "Bến xe Lạch Tray",
        "address": "Số 273 Trần Nguyên Hãn - Ngô Quyền - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Quận Ngô Quyền",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c2",
        "name": "Bến xe Đạ Tẻh",
        "address": "khu phố 3, thị trấn Đạ Tẻh, Đạ Tẻh, Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Huyện Đạ Tẻh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c3",
        "name": "Bến xe Ea Súp",
        "address": "Ea Súp, Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Ea Súp",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c4",
        "name": "Bến xe Kinh Cùng",
        "address": "Số 950 - Quốc lộ 61 Kinh Cùng - TX Ngã Bảy - Hậu Giang",
        "province": "Hậu Giang",
        "district": "Thành phố Ngã Bảy",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c5",
        "name": "Bến xe Long Mỹ",
        "address": "Trần Hưng Đạo - Long Mỹ - Hậu Giang",
        "province": "Hậu Giang",
        "district": "Thị xã Long Mỹ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c6",
        "name": "Bến xe Cái Tắc",
        "address": "Số 420 - Quốc lộ 1A - Cái Tắc - Châu Thành A - Hậu Giang",
        "province": "Hậu Giang",
        "district": "Huyện Châu Thành A",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c7",
        "name": "Bến xe Quận 8",
        "address": "Đường Tạ Quang Bửu - Phường 5 - Quận 8 - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 5",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c8",
        "name": "Bến xe Củ Chi",
        "address": "Quốc Lộ 22 - Thị trấn Củ Chi - Huyện Củ Chi - Phố Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Huyện Củ Chi",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79c9",
        "name": "Bến xe Ân Thi",
        "address": "Bùi Thị Cúc - Ân Thi - Hưng Yên",
        "province": "Hưng Yên",
        "district": "Thành phố Hưng Yên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ca",
        "name": "Bến xe Rạch Sỏi",
        "address": "Số 12 Mai Thị Hồng Hạnh - Rạch Giá - Kiên Giang",
        "province": "Kiên Giang",
        "district": "Thành phố Rạch Giá",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79cb",
        "name": "Bến xe An Minh",
        "address": "Quốc lộ 63 - Thứ Mười Một - An Minh - Kiên Giang",
        "province": "Kiên Giang",
        "district": "Huyện An Minh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79cc",
        "name": "Bến xe Ninh Hòa",
        "address": "Quốc lộ 1A - Xã Ninh Giang - Huyện Ninh Hòa - Ninh Hòa - Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Thị xã Ninh Hòa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79cd",
        "name": "Bến xe Hữu Lũng",
        "address": "Khu An Ninh - Huyện Hữu Lũng - Lạng Sơn",
        "province": "Lạng Sơn",
        "district": "Thành phố Lạng Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ce",
        "name": "Bến xe Văn Bàn",
        "address": "Thị trấn Khánh Yên - Huyện Văn Bàn - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79cf",
        "name": "Bến xe Đức Trọng",
        "address": "Số 735 Quốc lộ 20 - Thị trấn Liên Nghĩa - Đức Trọng - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Huyện Đức Trọng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d0",
        "name": "Bến xe Di Linh",
        "address": "Số 681 Hùng Vương - Thị trấn Di Linh - Di Linh - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Huyện Di Linh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d1",
        "name": "Bến xe Yên Định",
        "address": "Quốc lộ 21B - Yên Định - Hải Hậu - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d4",
        "name": "Bến xe thị trấn Hai Riêng",
        "address": "Thị trấn Hai Riêng - Huyện Sông Hinh - Phú Yên",
        "province": "Phú Yên",
        "district": "Huyện Sông Hinh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d2",
        "name": "Bến xe thị xã Cửa Lò",
        "address": "Số 35 Sào Nam - Nghi Thu - Thị xã Cửa Lò - Nghệ An",
        "province": "Nghệ An",
        "district": "Thị xã Cửa Lò",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d5",
        "name": "Bến xe La Hai",
        "address": "Số 166 Trần Hưng Đạo - Khu phố Long Châu - thị trấn La Hai - huyện Đồng Xuân - Phú Yên",
        "province": "Phú Yên",
        "district": "Huyện Đồng Xuân",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d3",
        "name": "Bến Xe Hạ Hòa",
        "address": "Tỉnh lộ 314, Thị trấn Hạ Hòa, Huyện Hạ Hòa, Tỉnh Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d6",
        "name": "Bến xe Quy Đạt",
        "address": "Thị trấn Quy Đạt - H. Minh Hóa - Quảng Bình",
        "province": "Quảng Bình",
        "district": "Huyện Minh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79dc",
        "name": "Bến xe bắc Quảng Ngãi",
        "address": "Thôn Liên Hiệp 2 - Thị trấn Sơn Tịnh - H. Sơn Tịnh - Quảng Ngãi",
        "province": "Quảng Ngãi",
        "district": "Thành phố Quảng Ngãi",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d7",
        "name": "Bến xe Hội An",
        "address": "Số 96 - Hùng Vương - Cẩm Phô - TP Hội An - Quảng Nam",
        "province": "Quảng Nam",
        "district": "Thành phố Hội An",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d9",
        "name": "Bến xe Thăng Bình",
        "address": "Thị trấn Hà Lam - H. Thăng Bình - Quảng Nam",
        "province": "Quảng Nam",
        "district": "Huyện Thăng Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79d8",
        "name": "Bến xe Tiên Phước",
        "address": "Thị trấn Tiên Kỳ - H. Tiên Phước - Quảng Nam",
        "province": "Quảng Nam",
        "district": "Huyện Tiên Phước",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79da",
        "name": "Bến xe Hòn Gai",
        "address": "Phường Hồng Hải - TP. Hạ Long - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Hạ Long",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79db",
        "name": "Bến xe Mông Dương",
        "address": "Quốc lộ 18 - Mông Dương - Cẩm Phả - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Cẩm Phả",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79de",
        "name": "Bến xe Khe Sanh",
        "address": "Ngô Sĩ Liên - Khe Sanh - Hướng Hóa - Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thị xã Quảng Trị",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e0",
        "name": "Bến xe Kế Sách",
        "address": "H. Kế Sách - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79dd",
        "name": "Bến xe phía bắc Đông Hà",
        "address": "Lê Duẩn - Đông Thanh - Đông Hà - Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thành phố Đông Hà",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79df",
        "name": "Bến xe Hải Lăng",
        "address": "Hùng Vương - Hải Lăng - Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thị xã Quảng Trị",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e1",
        "name": "Bến xe Trà Men",
        "address": "Số 27 Hùng Vương - Phường 6 - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e7",
        "name": "Bến xe Đông Hưng",
        "address": "Km 10 + 200 - Quốc lộ 10 - Đông Hợp - Đông Hưng - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e2",
        "name": "Bến xe Dương Minh Châu",
        "address": "Khu phố 1 - Thị trấn Dương Minh Châu - H. Dương Minh Châu - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e3",
        "name": "Bến xe Châu Thành (Tây Ninh)",
        "address": "Số 932 Hoàng Lê Kha - Thị trấn Châu Thành - H. Châu Thành - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e8",
        "name": "Bến Xe Tiền Hải",
        "address": "355 Phố Hùng Thắng, Tiền Hải, Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e5",
        "name": "Bến xe Nà Hang",
        "address": "Tổ 10 - H. Nà Hang - Tuyên Quang",
        "province": "Tuyên Quang",
        "district": "Thành phố Tuyên Quang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e4",
        "name": "Bến xe Gò Dầu",
        "address": "Quốc lộ 22 - Thị trấn Gò Dầu - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e6",
        "name": "Bến xe Hàm Yên",
        "address": "Tân Phú - Thị trấn Tân Yên - Hàm Yên - Tuyên Quang",
        "province": "Tuyên Quang",
        "district": "Thành phố Tuyên Quang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79e9",
        "name": "Bến xe Phổ Yên",
        "address": "Quốc lộ 3 - Ba Hàng - Phổ Yên - Thái Nguyên",
        "province": "Thái Nguyên",
        "district": "Thành phố Thái Nguyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ea",
        "name": "Bến xe Đại Từ",
        "address": "Quốc lộ 37 - H. Đại Từ - Thái Nguyên",
        "province": "Thái Nguyên",
        "district": "Thành phố Thái Nguyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79eb",
        "name": "Bến xe phía Tây",
        "address": "Nguyễn Trãi - Phường Phú Sơn - TP. Thanh Hóa - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ec",
        "name": "Bến xe Hoằng Hóa",
        "address": "Thị trấn Bút Sơn - H. Hoằng Hóa - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ef",
        "name": "Bến xe Nga Sơn",
        "address": "Quốc lộ 10 - Nga Sơn - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ed",
        "name": "Bến xe Cẩm Thủy",
        "address": "Cẩm Sơn - Cẩm Thủy - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ee",
        "name": "Bến xe Vĩnh Lộc",
        "address": "Quốc lộ 217 - Vĩnh Tiến - Vĩnh Lộc - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f0",
        "name": "Bến xe Nông Cống",
        "address": "Nam Giang - H. Nông Cống - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f1",
        "name": "Bến xe TP Huế",
        "address": "Số 6 Trần Hưng Đạo - TP. Huế - Thừa Thiên Huế",
        "province": "Thừa Thiên Huế",
        "district": "Thành phố Huế",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f2",
        "name": "Bến xe Thuận An",
        "address": "Số 12 Trấn Hải Thành - Thuận An - Phú Vang - Thừa Thiên Huế",
        "province": "Thừa Thiên Huế",
        "district": "Thành phố Huế",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f3",
        "name": "Bến xe Nguyễn Đáng",
        "address": "Phường 6 - Thị xã Trà Vinh - Trà Vinh",
        "province": "Trà Vinh",
        "district": "Thành phố Trà Vinh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f5",
        "name": "Bến xe Vũng Liêm",
        "address": "Tỉnh lộ 902 - Thị trấn Vũng Liêm - Vũng Liêm - Vĩnh Long",
        "province": "Vĩnh Long",
        "district": "Huyện  Vũng Liêm",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f4",
        "name": "Bến xe TP Vĩnh Long",
        "address": "Đường Ba Tháng Hai - Phường 1 - TP. Vĩnh Long - Vĩnh Long",
        "province": "Vĩnh Long",
        "district": "Thành phố Vĩnh Long",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f6",
        "name": "Bến Xe Lập Thạch",
        "address": "Thị trấn Lập Thạch, Huyện Lập Thạch, Tỉnh Vĩnh Phúc",
        "province": "Vĩnh Phúc",
        "district": "Huyện Lập Thạch",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f7",
        "name": "Bến xe Phúc Yên",
        "address": "Hai Bà Trưng - Phúc Yên - Vĩnh Phúc",
        "province": "Vĩnh Phúc",
        "district": "Thành phố Phúc Yên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f8",
        "name": "Bến xe Lục Yên",
        "address": "Khu 3 - Yên Thế - Lục Yên -Yên Bái",
        "province": "Yên Bái",
        "district": "Thành phố Yên Bái",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79f9",
        "name": "Bến Xe Gia Bình",
        "address": "Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
        "province": "Bắc Ninh",
        "district": "Thành phố Bắc Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79fa",
        "name": "Bến Xe Khách Huyện Bắc Quang",
        "address": "Quốc lộ 2, Thị trấn Việt Quang - Bắc Quang - Hà Giang",
        "province": "Hà Giang",
        "district": "Thành phố Hà Giang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79fb",
        "name": "Bến xe Hưng Hòa",
        "address": "Thị trấn Hưng Hòa - Hưng Hà - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79fc",
        "name": "Bến xe Diêm Điền 1",
        "address": "Thị trấn Diêm Điền - Thái Thụy - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a06",
        "name": "Bến Xe Yên Thành",
        "address": "Khối 1 - thị trấn Yên Thành - Yên Thành - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Yên Thành",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a05",
        "name": "Bến Xe Ngọc Lặc",
        "address": "Thị Trấn Ngọc Lặc - Ngọc Lặc - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a01",
        "name": "Bến Xe khách huyện Tuần Giáo",
        "address": "Khu phố 3 - Tuần Giáo - Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a00",
        "name": "Bến Xe Sim",
        "address": "Xã Hợp Thành - Triệu Sơn - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79fd",
        "name": "Bến Xe Quảng Xương",
        "address": "Quảng Xương - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79fe",
        "name": "Bến xe Minh Lộc",
        "address": "Xã Minh Lộc - Hậu Lộc - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a04",
        "name": "Bến Xe Yên Châu",
        "address": "Thị trấn Yên Châu - Yên Châu - Sơn La",
        "province": "Sơn La",
        "district": "Thành phố Sơn La",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a02",
        "name": "Bến xe Tân Hà",
        "address": "Xã Tân Hà - Lâm Hà - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Huyện Lâm Hà",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a79ff",
        "name": "Bến xe Đoan Hùng",
        "address": "Đoan Hùng - Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a03",
        "name": "Bến Xe Tam Điệp",
        "address": "Nút giao quốc lộ 12B và Quốc lộ 1A - Tam Điệp - Ninh Bình",
        "province": "Ninh Bình",
        "district": "Thành phố Ninh Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0e",
        "name": "Bến Xe Yên Mỹ",
        "address": "Thị trấn Yên Mỹ - Nông Cống - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0c",
        "name": "Bến Xe Bắc Vinh",
        "address": "Xóm 1, xã Nghi Kim, thành phố Vinh, tỉnh Nghệ An",
        "province": "Nghệ An",
        "district": "Thành phố Vinh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a10",
        "name": "Bến xe Thạnh Trị",
        "address": "Thị trấn Phú Lộc - H. Thạnh Trị - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a08",
        "name": "Bến xe Pác Nặm",
        "address": "Thôn Nà Coóc, xã Bộc Bố, huyện Pác Nặm, tỉnh Bắc Kạn.",
        "province": "Bắc Kạn",
        "district": "Thành Phố Bắc Kạn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a07",
        "name": "Bến xe Đông Triều",
        "address": "Khu 4 - Thị trấn Đông Triều - H. Đông Triều - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thị xã Đông Triều",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0a",
        "name": "Bến Xe Tân Kì",
        "address": "Thị trấn Tân Kỳ - huyện Tân Kỳ - tỉnh Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Tân Kỳ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a09",
        "name": "Bến Xe Khách Tỉnh Ninh Thuận",
        "address": "Quốc Lộ 1A, Lê Duẩn - Phan Rang-Tháp Chàm - Ninh Thuận",
        "province": "Ninh Thuận",
        "district": "Thành phố Phan Rang-Tháp Chàm",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0b",
        "name": "Bến Xe Lý Nhân",
        "address": "Số 225 Trần Hưng Đạo - Đông Lý - Lý Nhân - Hà Nam",
        "province": "Hà Nam",
        "district": "Huyện Lý Nhân",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0f",
        "name": "Bến Xe Khách Than Uyên",
        "address": "Khu 10 - thị trấn Than Uyên - Than Uyên - Lai Châu",
        "province": "Lai Châu",
        "district": "Thành phố Lai Châu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a0d",
        "name": "Bến Xe Như Thanh",
        "address": "Thị trấn Như Thanh - Như Thanh - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a11",
        "name": "Bến xe Thanh Chương",
        "address": "Quốc lộ 46 - Thị trấn Thanh Chương - Thanh Chương - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Thanh Chương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a12",
        "name": "Bến Xe Trân Bảo Trân",
        "address": "391 Đinh Bộ Lĩnh - Phường 26 - Bình Thạnh - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 1",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a13",
        "name": "Bến xe Krông Bông",
        "address": "Tỉnh lộ 12, Krông Bông - Krông Bông - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Krông Bông",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a14",
        "name": "Bến xe Krông Năng",
        "address": "Số 45, Nguyễn Tất Thành - Krông Năng - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Krông Năng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a19",
        "name": "Bến xe Cửa Ông",
        "address": "Tổ 18 - Phường Cửa Ông - Thị xã Cẩm Phả - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Cẩm Phả",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405154657_1-quang_x20_ninh-ben-xe-cua-ong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a16",
        "name": "Bến xe Hồng Ngự",
        "address": "Ấp An Lộc - Xã An Bình A  - Hồng Ngự - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Thành phố Hồng Ngự",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a17",
        "name": "Bến xe Nho Quan",
        "address": "Tỉnh lộ 477 - Thị trấn Nho Quan - Nho Quan - Ninh Bình",
        "province": "Ninh Bình",
        "district": "Thành phố Ninh Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a15",
        "name": "Bến xe Trung tâm Thị Xã Cẩm Phả",
        "address": "Cẩm Phả - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Cẩm Phả",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1409799946_1-quang_x20_ninh-ben_x20_xe_x20_cam_x20_pha.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1a",
        "name": "Bến xe Giao Thủy",
        "address": "Km 16+350 Tỉnh lộ 489, Thị trấn Ngô Đồng - Giao Thủy - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a18",
        "name": "Bến Xe Chợ Lớn",
        "address": "86 Trang Tử, Phường 2 - Quận 5 - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 5",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408518869_2-hcm-ben_x20_xe_x20_cho_x20_lon.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1b",
        "name": "Bến Xe Tri Tôn",
        "address": "Thị Trấn Tri Tôn - Huyện Tri Tôn - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Tri Tôn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1c",
        "name": "Bến xe Phù Cát",
        "address": "Phù Cát, Bình Định",
        "province": "Bình Định",
        "district": "Huyện Phù Cát",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1d",
        "name": "Bến xe Huệ Thanh",
        "address": "Vị Thanh - Hậu Giang",
        "province": "Hậu Giang",
        "district": "Thành phố Vị Thanh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1f",
        "name": "Bến xe Hùng Vương (Cần Thơ)",
        "address": "Số 01 Hùng Vương - Thới Bình - Q.Ninh Kiều - TP. Cần Thơ - Tỉnh Cần Thơ",
        "province": "Cần Thơ",
        "district": "Quận Ninh Kiều",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a1e",
        "name": "Bến xe Pleiku",
        "address": "43 Lý Nam Đế - Pleiku - Gia Lai",
        "province": "Gia Lai",
        "district": "Thành phố Pleiku",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a20",
        "name": "Bến xe Đồng Quang",
        "address": "P. Quang Trung - Thái Nguyên - Thái Nguyên",
        "province": "Thái Nguyên",
        "district": "Thành phố Thái Nguyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a21",
        "name": "Bến Xe Tân Châu",
        "address": "Thị xã Tân Châu - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Thị xã Tân Châu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a23",
        "name": "Bến xe Hà Tĩnh",
        "address": "15 Đường Trần Phú, Phường Trần Phú - Hà Tĩnh - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405140647_1-ha-tinh-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a22",
        "name": "Bến xe Thạnh Phú",
        "address": "Ấp 10 Đường Quốc Lộ 57 , Thị Trấn Thạnh Phú - Thạnh Phú - Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408516121_1-an_x20_giang-ben_x20_xe_x20_thanh_x20_phu.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a24",
        "name": "Bến xe Bờ Sông",
        "address": "397 Đinh Bộ Lĩnh - Bình Thạnh - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận Bình Thạnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a25",
        "name": "Bến Xe Chợ Mới",
        "address": "Huyện Chợ Mới, Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Chợ Mới",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a27",
        "name": "Bến xe Sa Pa",
        "address": "Sa Pa - Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408516493_1-sapa-ben_x20_xe_x20_sa_x20_pa.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a26",
        "name": "Bến xe Hữu  nghị Quan",
        "address": "Cao Lộc,Lạng Sơn",
        "province": "Lạng Sơn",
        "district": "Thành phố Lạng Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2f",
        "name": "Bến Xe khách thị trấn Thọ Xuân",
        "address": "Thị trấn thọ xuân - Thọ Xuân - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a28",
        "name": "Bến xe Tiên Yên",
        "address": "Tiên Yên Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Huyện Tiên Yên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a29",
        "name": "Bến Xe Cờ Đỏ",
        "address": "Tỉnh Lộ 922, Ấp Thới Thuận, Cờ Đỏ, TP.Cần Thơ",
        "province": "Cần Thơ",
        "district": "Huyện Cờ Đỏ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2a",
        "name": "Bến xe khách Ô Môn",
        "address": "KV5 - Quốc Lộ 91 - Q. Ô Môn - TP Cần Thơ - Tỉnh Cần Thơ",
        "province": "Cần Thơ",
        "district": "Quận Ô Môn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a30",
        "name": "Bến Xe Bến Cát",
        "address": "KCN-TĐC Mỹ Phước I - Ấp 6 - Xã Thới Hòa - Bến Cát - Bình Dương",
        "province": "Bình Dương",
        "district": "Thị xã Bến Cát",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2c",
        "name": "Bến xe Rạng Đông",
        "address": "Đông Bình - Nghĩa Hưng - Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2b",
        "name": "Bến Xe Bình Định",
        "address": "Ngã 5 Tây Sơn, Phường Ghềnh Ráng, TP Quy Nhơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Thành phố Quy Nhơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2d",
        "name": "Bến Xe Phía Nam Buôn Mê Thuột",
        "address": "Phường Khánh Xuân - Buôn Ma Thuột - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Thành phố Buôn Ma Thuột",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408502794_1-bmt-ben_x20_xe_x20_phia_x20_nam.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a31",
        "name": "Bến Xe Lam Hồng",
        "address": "Quốc lộ 1A - Xã An Bình - Dĩ An - Bình Dương",
        "province": "Bình Dương",
        "district": "Thành phố Dĩ An",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405140938_1-nam-hong-binh-duong-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a2e",
        "name": "Bến Xe Quỳnh Côi",
        "address": "217, tt. Quỳnh Côi - Quỳnh Phụ - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a32",
        "name": "Bến xe Nghĩa Đàn",
        "address": "Thị xã Thái Hòa - Nghĩa Đàn - Nghệ An",
        "province": "Nghệ An",
        "district": "Huyện Nghĩa Đàn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a38",
        "name": "Bến xe Sông Cầu",
        "address": "Thị trấn Sông Cầu - Sông Cầu - Phú Yên",
        "province": "Phú Yên",
        "district": "Thị xã Sông Cầu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3a",
        "name": "Bến Xe An Sương",
        "address": "Quốc lộ 22, Xã Bà Điểm, H.Hóc Môn, TP.Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Huyện Hóc Môn",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405134452_1-ben-xe-an-suong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a33",
        "name": "Bến xe Xuân Lộc",
        "address": "Thị Trấn Gia Ray - Xuân Lộc - Tỉnh Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Xuân Lộc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a34",
        "name": "Bến Xe Cam Ranh",
        "address": "01 Đường số 7 - Phường Cam Lộc - Cam Ranh - Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Thành phố Cam Ranh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408517586_2-khanh_x20_hoa-ben_x20_xe_x20_cam_x20_ranh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a35",
        "name": "Bến xe Hương Sơn",
        "address": "Quốc lộ 8A - Thị Trấn Phố Châu - Hương Sơn - Hà Tĩnh",
        "province": "Hà Tĩnh",
        "district": "Thành phố Hà Tĩnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a36",
        "name": "Bến Xe Phú Tân",
        "address": "Tỉnh lộ 954 - Huyện Phú Tân - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Huyện Phú Tân",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a37",
        "name": "Bến xe Triều Dương",
        "address": "Tiên Lữ - Hưng Yên",
        "province": "Hưng Yên",
        "district": "Thành phố Hưng Yên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405138871_1-hung-yen-ben-xe-trieu-duong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a39",
        "name": "Bến xe Đức Long Bảo Lộc",
        "address": "Trần Phú, phường Lộc Sơn - Bảo Lộc - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Thành phố Bảo Lộc",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405133747_1-lam-dong-ben-xe-khach-duc-long-bao-loc.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3b",
        "name": "Bến xe Niệm Nghĩa",
        "address": "Số 273 Trần Nguyên Hãn - Phường Nghĩa Xá - Lê Chân - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Quận Lê Chân",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402286844_4.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3c",
        "name": "Bến xe Gò Công",
        "address": "Đường Đồng Khởi - Phường 4 - Gò Công - Tiền Giang",
        "province": "Tiền Giang",
        "district": "Thị xã Gò Công",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408507635_1-tiengiang-ben_x20_xe_x20_go_x20_cong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3d",
        "name": "Bến xe Đức Long Gia Lai",
        "address": "43 Lý Nam Đế - Pleiku - Gia Lai",
        "province": "Gia Lai",
        "district": "Thành phố Pleiku",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408509581_1-gia_x20_lai-ben_x20_xe_x20_duc_x20_long_x20_gia_x20_lai.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3f",
        "name": "Bến xe Hà Tiên",
        "address": "Quốc lộ 80, khu phố 5 - phường Bình San - thị xã Hà Tiên - tỉnh Kiên Giang",
        "province": "Kiên Giang",
        "district": "Thành phố Hà Tiên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408514773_1-ha_x20_tien-ben_x20_xe_x20_ha_x20_tien.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a3e",
        "name": "Bến xe Sơn La",
        "address": "Tổ 1 - Phường Quyết Tâm - TP Sơn La - Tỉnh Sơn La",
        "province": "Sơn La",
        "district": "Thành phố Sơn La",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408511508_4-son_x20_la-ben_x20_xe_x20_son_x20_la.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a41",
        "name": "Bến xe Tây Ninh",
        "address": "KP1 - Trưng Nữ Vương - Phường 2 - TX Tây Ninh - Tỉnh Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a40",
        "name": "Bến xe Việt Trì",
        "address": "Số 1506 Hùng Vương - TP Việt Trì - Tỉnh Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thành phố Việt Trì",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a43",
        "name": "Bến Xe Bắc Kạn",
        "address": "Tổ 8, Phường Đức Xuân, Thị xã Bắc Kạn, Tỉnh Bắc Kạn",
        "province": "Bắc Kạn",
        "district": "Thành Phố Bắc Kạn",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408508871_1-bac_x20_kan-ben_x20_xe_x20_bac_x20_kan.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a44",
        "name": "Bến xe Bắc Ninh",
        "address": "Đường Nguyễn Du - P.Ninh Xá - TP.Bắc Ninh - Tỉnh Bắc Ninh",
        "province": "Bắc Ninh",
        "district": "Thành phố Bắc Ninh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405155901_1-bac-ninh-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a42",
        "name": "Bến xe Bạc Liêu",
        "address": "Trần Phú - TP Bạc Liêu - Bạc Liêu",
        "province": "Bạc Liêu",
        "district": "Thành phố Bạc Liêu",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408508628_1-bac_x20_lieu-ben_x20_xe_x20_bac_x20_lieu.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a45",
        "name": "Bến Xe Bình Dương",
        "address": "Đường Ba Mươi Tháng Tư, Phường Chánh Nghĩa, Thủ Dầu Một, Bình Dương",
        "province": "Bình Dương",
        "district": "Thành phố Thủ Dầu Một",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408509034_1-binh_x20_duong-ben_x20_xe_x20_binh_x20_duong.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a46",
        "name": "Bến xe Gia Lai",
        "address": "Đường Trường Chinh - TX.Pleiku - Tỉnh Gia Lai",
        "province": "Gia Lai",
        "district": "Thành phố Pleiku",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a47",
        "name": "Bến xe TX Phủ Lý",
        "address": "Đường Đinh Tiên Hoàng - P.Trần Hưng Đạo - TP.Phủ Lý - Tỉnh Hà Nam",
        "province": "Hà Nam",
        "district": "Thành phố Phủ Lý",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a48",
        "name": "Bến xe Vị Thanh",
        "address": "Trần Hưng Đạo - Phường 5 - Vị Thanh - Hậu Giang",
        "province": "Hậu Giang",
        "district": "Thành phố Vị Thanh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408509347_1-hau_x20_giang-ben_x20_xe_x20_vi_x20_thanh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a49",
        "name": "Bến xe KBang",
        "address": "Số 07 Lê Lợi - KBang - Gia Lai",
        "province": "Gia Lai",
        "district": "Huyện KBang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4a",
        "name": "Bến xe 91B Cần Thơ",
        "address": "Lô 91B - Nguyễn Văn Linh - P.Hưng Lợi - Q.Ninh Kiều - TP.Cần Thơ - Tỉnh Cần Thơ",
        "province": "Cần Thơ",
        "district": "Quận Ninh Kiều",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402116152_3.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4b",
        "name": "Bến xe Kiến Xương",
        "address": "Thị trấn Thanh Nê - Kiến Xương - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4c",
        "name": "Bến xe Móng Cái",
        "address": "Phường Trần Phú - TP Móng Cái - Quảng Ninh",
        "province": "Quảng Ninh",
        "district": "Thành phố Móng Cái",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408504186_1-quang_x20_ninh-ben_x20_xe_x20_mong_x20_cai.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4d",
        "name": "Bến Xe Tân Uyên",
        "address": "Tân Uyên - Bình Dương",
        "province": "Bình Dương",
        "district": "Thị xã Tân Uyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4e",
        "name": "Bến Xe Phước Long (Bình Phước)",
        "address": "Số 56, Tỉnh lộ 741, Thị trấn Thác Mơ, Huyện Phước Long, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Thị xã Phước Long",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a50",
        "name": "Bến xe Buôn Mê Thuột",
        "address": "Ngô Gia Tự - Buôn Ma Thuột - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Thành phố Buôn Ma Thuột",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a4f",
        "name": "Bến Xe Tây Sơn",
        "address": "Quốc lộ 19, Thị trấn Phú Phong, Huyện Tây Sơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Huyện Tây Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a51",
        "name": "Bến xe Thành Thắng",
        "address": "Sầm Sơn - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a52",
        "name": "Bến Xe Bắc Giang",
        "address": "Số 197 Xương Giang, P.Thọ Xương, TP.Bắc Giang, Tỉnh Bắc Giang",
        "province": "Bắc Giang",
        "district": "Thành phố Bắc Giang",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405156601_1-bac-giang-ben-xe-khach.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a55",
        "name": "Bến xe Kon Tum",
        "address": "110 Trần Hưng Đạo - TX.Kon Tum - Tỉnh Kon Tum",
        "province": "Kon Tum",
        "district": "Thành phố Kon Tum",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a54",
        "name": "Bến xe Rạch Giá",
        "address": "Số 260A Nguyễn Bỉnh Khiêm - TP. Rạch Giá - Tỉnh Kiên Giang",
        "province": "Kiên Giang",
        "district": "Thành phố Rạch Giá",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408510386_1-rach_x20_gia-ben_x20_xe_x20_rach_x20_gia.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a53",
        "name": "Bến xe tỉnh Lai Châu",
        "address": "Đường 4D - Tổ 1 - P.Tân Phong - TX.Lai Châu - Tỉnh Lai Châu",
        "province": "Lai Châu",
        "district": "Thành phố Lai Châu",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408504880_1-lai_x20_chau-ben_x20_xe_x20_lai_x20_chau.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a59",
        "name": "Bến xe Ninh Bình",
        "address": "Số 207 Lê Đại Hành - P.Thanh Bình - TP Ninh Bình - Tỉnh Ninh Bình",
        "province": "Ninh Bình",
        "district": "Thành phố Ninh Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a56",
        "name": "Bến xe Lào Cai",
        "address": "Minh Khai - P. Phố Mới - TP. Lào Cai - Tỉnh Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408510555_1-lao_x20_cai-ben_x20_xe_x20_lao_x20_cai.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a57",
        "name": "Bến xe Nam Định",
        "address": "Xã Lộc Hòa - TP.Nam Định - Tỉnh Nam Định",
        "province": "Nam Định",
        "district": "Thành phố Nam Định",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a58",
        "name": "Bến xe Vinh",
        "address": "Số 77 Lê Lợi - TP Vinh - Tỉnh Nghệ An",
        "province": "Nghệ An",
        "district": "Thành phố Vinh",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405155141_2-vinh-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5a",
        "name": "Bến xe Tam Kỳ",
        "address": "Số 1 Phan Bội Châu - Phường Tân Thanh - TX Tam Kỳ - Tỉnh Quảng Nam",
        "province": "Quảng Nam",
        "district": "Thành phố Tam Kỳ",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408511095_1-quang_x20_nam-ben_x20_xe_x20_tam_x20_ky.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5b",
        "name": "Bến xe Đông Hà",
        "address": "Số 68 Lê Duẩn - TX Đông Hà - Tỉnh Quảng Trị",
        "province": "Quảng Trị",
        "district": "Thành phố Đông Hà",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408511239_2-quang_x20_tri-ben_x20_xe_x20_dong_x20_ha.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5c",
        "name": "Bến xe Sóc Trăng",
        "address": "Số 38 Lê Duẩn - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405155566_1-soc-trang-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a60",
        "name": "Bến xe phía bắc TP Huế",
        "address": "Lý Thái Tổ - TP. Huế - Thừa Thiên Huế",
        "province": "Thừa Thiên Huế",
        "district": "Thành phố Huế",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5d",
        "name": "Bến xe phía bắc Thanh Hóa",
        "address": "Số 418 Bà Triệu - Phường Đông Thọ - TP Thanh Hóa - Tỉnh Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5f",
        "name": "Bến xe Thái Nguyên",
        "address": "Phường Quang Trung - TP Thái Nguyên - Tỉnh Thái Nguyên",
        "province": "Thái Nguyên",
        "district": "Thành phố Thái Nguyên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408511915_1-thai_x20_nguyen-ben_x20_xe_x20_thai_x20_nguyen.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a5e",
        "name": "Bến xe phía nam Thanh Hóa",
        "address": "QL1A - Phường Đông Vệ - TP Thanh Hóa - Tỉnh Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a61",
        "name": "Bến xe Trà Vinh",
        "address": "Số 559, QL54 - Khóm 4 - Phường 9 - TP Trà Vinh - Trà Vinh",
        "province": "Trà Vinh",
        "district": "Thành phố Trà Vinh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6b",
        "name": "Bến xe Ba Đồn",
        "address": "Khu Phố 1, Thị Xã Ba Đồn - Quảng Trạch - Quảng Bình",
        "province": "Quảng Bình",
        "district": "Huyện Quảng Trạch",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a65",
        "name": "Bến Xe Tam Nông",
        "address": "Trần Hưng Đạo - Tràm Chim - Tam Nông - Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Huyện Tam Nông",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a64",
        "name": "Bến xe Quảng Ngãi",
        "address": "Số 26 Lê Thánh Tôn - Phường Nghĩa Chánh - TP Quảng Ngãi - Quảng Ngãi",
        "province": "Quảng Ngãi",
        "district": "Thành phố Quảng Ngãi",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a66",
        "name": "Bến xe Ayun Pa",
        "address": "313 Trần Hưng Đạo, Phường Hòa Bình - Ayun Pa - Gia Lai",
        "province": "Gia Lai",
        "district": "Thị xã Ayun Pa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a67",
        "name": "Bến xe Krông Pa",
        "address": "Thị Trấn Phú Túc - Krông Pa - Gia Lai",
        "province": "Gia Lai",
        "district": "Huyện Krông Pa",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408517909_1-daklak-ben_x20_xe_x20_krongpa.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a68",
        "name": "Bến xe Bù Gia Mập",
        "address": "Bù Gia Mập, Bù Gia Mập, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Bù Gia Mập",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6a",
        "name": "Bến xe Mường La",
        "address": "Mường La - Sơn La",
        "province": "Sơn La",
        "district": "Thành phố Sơn La",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a69",
        "name": "Bến xe Biên Hòa",
        "address": "Số 4 Nguyễn Ái Quốc - P.Quang Vinh - TP.Biên Hòa - Tỉnh Đồng Nai",
        "province": "Đồng Nai",
        "district": "Thành phố Biên Hòa",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408506876_2-bien_x20_hoa-ben_x20_xe_x20_bien_x20_hoa.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a62",
        "name": "Bến xe Vĩnh Long",
        "address": "QL1A - TX Vĩnh Long - Tỉnh Vĩnh Long",
        "province": "Vĩnh Long",
        "district": "Thành phố Vĩnh Long",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408513478_1-vinh_x20_long-ben_x20_xe_x20_vinh_x20_long.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a63",
        "name": "Bến xe Yên Bái",
        "address": "Phường Nguyễn Thái Học - TP Yên Bái - Tỉnh Yên Bái",
        "province": "Yên Bái",
        "district": "Thành phố Yên Bái",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a72",
        "name": "Bến xe thị xã Tây Ninh",
        "address": "Số 136 - Khu phố 1 - Phường 2 - Tây Ninh",
        "province": "Tây Ninh",
        "district": "Thành phố Tây Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a75",
        "name": "Bến xe Thanh Ba",
        "address": "Tỉnh lộ 311 - Thanh Ba - Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a73",
        "name": "Bến xe Thảo Quyên",
        "address": "294 Rồng Đen - Tân Bình - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận Tân Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6d",
        "name": "Bến Xe Thái Bình",
        "address": "Tỉnh lộ 223, TP.Thái Bình, Tỉnh Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6c",
        "name": "Bến Xe Gò Bồi",
        "address": "Huyện Tuy Phước, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Huyện Tuy Phước",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6e",
        "name": "Bến xe Tân Thanh",
        "address": "Tân Thanh - Vãn Lãng - Lạng Sơn",
        "province": "Lạng Sơn",
        "district": "Thành phố Lạng Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a71",
        "name": "Bến xe Tân Phú",
        "address": "Km 125 - Quốc Lộ 20 – Tân Phú - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Tân Phú",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a6f",
        "name": "Bến Xe Bồng Sơn",
        "address": "Thị trấn Bồng Sơn, Huyện Hoài Nhơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Thị xã Hoài Nhơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a70",
        "name": "Bến xe Điện Biên",
        "address": "Phố 1, Phường Thanh Bình - Điện Biên Phủ - Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a74",
        "name": "Bến xe Quảng Trường Gia",
        "address": "Lào Cai",
        "province": "Lào Cai",
        "district": "Thành phố Lào Cai",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a76",
        "name": "Bến Xe Châu Đốc",
        "address": "Đường Lê Lợi - Phường Châu Phú B - TP Châu Đốc - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Thành phố Châu Đốc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a77",
        "name": "Bến xe Cần Thơ",
        "address": "Nguyễn Trãi - P. An Hội - Q. Ninh Kiều - TP. Cần Thơ - Tỉnh Cần Thơ",
        "province": "Cần Thơ",
        "district": "Quận Ninh Kiều",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405138508_3-can-tho-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a78",
        "name": "Bến xe Sông Ray",
        "address": "Huyện Cẩm Mỹ - Cẩm Mỹ - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Cẩm Mỹ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a79",
        "name": "Bến xe Vĩnh Châu",
        "address": "Thị xã Vĩnh Châu - Vĩnh Châu - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7a",
        "name": "Bến xe Phan Thiết",
        "address": "Trần Quý Cáp, Phường Đức Long - Phan Thiết - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thành phố Phan Thiết",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7b",
        "name": "Bến xe Lạng Sơn",
        "address": "Số 28 Ngô Quyền - TP.Lạng Sơn - Tỉnh Lạng Sơn",
        "province": "Lạng Sơn",
        "district": "Thành phố Lạng Sơn",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7e",
        "name": "Bến xe TP Tuyên Quang",
        "address": "Tp Tuyên Quang - Tỉnh Tuyên Quang",
        "province": "Tuyên Quang",
        "district": "Thành phố Tuyên Quang",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408514253_3-tuyen_x20_quang-ben_x20_xe_x20_tuyen_x20_quang.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7c",
        "name": "Bến xe Đồng Hới",
        "address": "Trần Hưng Đạo - Phường Đồng Phú - TX Đồng Hới - Tỉnh Quảng Bình",
        "province": "Quảng Bình",
        "district": "Thành Phố Đồng Hới",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408514002_1-quang_x20_binh-ben_x20_xe_x20_dong_x20_hoi.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7d",
        "name": "Bến xe phía Nam Thừa Thiên Huế",
        "address": "Số 97A đường An Dương Vương - TP Huế - Tỉnh Thừa Thiên Huế",
        "province": "Thừa Thiên Huế",
        "district": "Thành phố Huế",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a7f",
        "name": "Bến xe Phía Nam Buôn Ma Thuột",
        "address": "Số 186 Nguyễn Tất Thành - Buôn Ma Thuột - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Thành phố Buôn Ma Thuột",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a80",
        "name": "Bến xe Đắk Hà",
        "address": "Số 296 đường Hùng Vương, Thị Trấn Đắk Hà - Đắk Hà - Kon Tum",
        "province": "Kon Tum",
        "district": "Thành phố Kon Tum",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a81",
        "name": "Bến xe Phía Nam Nha Trang",
        "address": "Số 58 Đường 23/10, TP.Nha Trang, Tỉnh Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Thành phố Nha Trang",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408514508_1-nha_x20_trang-ben_x20_xe_x20_nam_x20_nha_x20_trang.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a82",
        "name": "Bến xe Ba Hòn",
        "address": "Quốc Lộ 80, Xã Dương Hòa - Kiên Lương - Kiên Giang",
        "province": "Kiên Giang",
        "district": "Huyện Kiên Lương",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a83",
        "name": "Bến xe Vạn Giã",
        "address": "Lê Hồng Phong - Vạn Giã - Vạn Ninh - Khánh Hòa",
        "province": "Khánh Hòa",
        "district": "Huyện Vạn Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a84",
        "name": "Bến xe Hưng Yên",
        "address": "QL 39, P. Hiến Nam, TP. Hưng Yên, Tỉnh Hưng Yên",
        "province": "Hưng Yên",
        "district": "Thành phố Hưng Yên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408515200_4-hung_x20_yen-ben_x20_xe_x20_hung_x20_yen.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a85",
        "name": "Bến Xe Quy Nhơn",
        "address": "Số 71 Tây Sơn, TP Quy Nhơn, Tỉnh Bình Định",
        "province": "Bình Định",
        "district": "Thành phố Quy Nhơn",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408515490_1-quy_x20_nhon-ben_x20_xe_x20_quy_x20_nhon.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a86",
        "name": "Bến Xe Liên Tỉnh Đắk Lắk",
        "address": "Km 4, Đường Nguyễn Chí Thanh, Phường Tân An, Buôn Ma Thuột, Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Thành phố Buôn Ma Thuột",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408515695_1-daklak-ben_x20_xe_x20_lien_x20_tinh.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a88",
        "name": "Bến xe Chư Sê",
        "address": "Ngã ba Cheo Reo - Chư Sê - Gia Lai",
        "province": "Gia Lai",
        "district": "Huyện Chư Sê",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a87",
        "name": "Bến xe An Khê",
        "address": "523 Quang Trung - P. Tây Sơn - An Khê - Gia Lai",
        "province": "Gia Lai",
        "district": "Thị xã An Khê",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a89",
        "name": "Bến xe huyện Đắk Đoa",
        "address": "Số 426 Nguyễn Huệ - Đăk Đoa - Gia Lai",
        "province": "Gia Lai",
        "district": "Huyện Đăk Đoa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8c",
        "name": "Bến Xe Quang Vinh",
        "address": "Ấp 1, Xã Hội Nghĩa - Tân Uyên - Bình Dương",
        "province": "Bình Dương",
        "district": "Thị xã Tân Uyên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8a",
        "name": "Bến Xe Khách Bảo Lộc",
        "address": "284 Trần Phú, Lộc Sơn - Bảo Lộc - Lâm Đồng",
        "province": "Lâm Đồng",
        "district": "Thành phố Bảo Lộc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8b",
        "name": "Bến xe Buôn Hồ",
        "address": "Km 33 - Quốc lộ 14 - Phường Thống Nhất - Buôn Hồ - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Thị xã Buôn Hồ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8d",
        "name": "Bến xe Nam Hải",
        "address": "Tiền Hải - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8e",
        "name": "Bến xe Tuấn Hoa",
        "address": "76 Lê Lai - Quận 1 - Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 1",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a8f",
        "name": "Bến xe Thường Xuân",
        "address": "Thường Xuân - Thanh Hóa",
        "province": "Thanh Hóa",
        "district": "Thành phố Thanh Hóa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a94",
        "name": "Bến Xe Lộc Ninh",
        "address": "Ấp 1, Thị trấn Lộc Ninh, Huyện Lộc Ninh, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Lộc Ninh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a90",
        "name": "Bến xe Mỹ Quới",
        "address": "Mỹ Quới - Ngã Năm - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a92",
        "name": "Bến xe An Phú",
        "address": "Ấp 1A - Xã An Phú - Thuận An - Bình Dương",
        "province": "Bình Dương",
        "district": "Thành phố Thuận An",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a91",
        "name": "Bến xe Phú Lộc",
        "address": "Quốc lộ 1A, Phú Lộc - Thạnh Trị - Sóc Trăng",
        "province": "Sóc Trăng",
        "district": "Thành phố Sóc Trăng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a97",
        "name": "Bến xe Ea HLeo",
        "address": "Thị trấn Ea Drăng - Ea Hleo - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Lắk",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a93",
        "name": "Bến xe thành phố Hà Giang",
        "address": "Đường 19/5 - Phường Nguyễn Trãi - Hà Giang - Hà Giang",
        "province": "Hà Giang",
        "district": "Thành phố Hà Giang",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a98",
        "name": "Bến xe phía nam TP Huế",
        "address": "Số 97A An Dương Vương - TP Huế - Thừa Thiên Huế",
        "province": "Thừa Thiên Huế",
        "district": "Thành phố Huế",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a95",
        "name": "Bến xe thị xã Lai Châu",
        "address": "Thị xã Lai Châu - Tỉnh Lai Châu",
        "province": "Lai Châu",
        "district": "Thành phố Lai Châu",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a96",
        "name": "Bến xe Phú Thọ",
        "address": "Bạch Đằng - Phú Thọ - Phú Thọ",
        "province": "Phú Thọ",
        "district": "Thị xã Phú Thọ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9a",
        "name": "Bến xe Thái Thụy",
        "address": "Thái Thụy - Thái Bình",
        "province": "Thái Bình",
        "district": "Thành phố Thái Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a99",
        "name": "Bến xe Phù Mỹ",
        "address": "Phù Mỹ - Bình Định",
        "province": "Bình Định",
        "district": "Huyện Phù Mỹ",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9b",
        "name": "Bến xe Mũi Né",
        "address": "Huỳnh Thúc Kháng - Mũi Né - Phan Thiết - Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thành phố Phan Thiết",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9f",
        "name": "Bến xe Nam Phan Thiết",
        "address": "Số 666 Trần Hưng Đạo - TP. Phan Thiết - Tỉnh Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thành phố Phan Thiết",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa0",
        "name": "Bến xe Cao Bằng",
        "address": "Thôn Tam Trung - P.Sông Bằng - TX.Cao Bằng - Tỉnh Cao Bằng",
        "province": "Cao Bằng",
        "district": "Thành phố Cao Bằng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9c",
        "name": "Bến xe Đại Lộc",
        "address": "14B, Ái Nghĩa - Đại Lộc - Quảng Nam",
        "province": "Quảng Nam",
        "district": "Huyện Đại Lộc",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9d",
        "name": "Bến Xe Đồng Xoài",
        "address": "Khu phố Tân Trà, Phú Riềng Đỏ, Phường Tân Xuân, Thị xã Đồng Xoài, Tỉnh Bình Phước",
        "province": "Bình Phước",
        "district": "Huyện Phú Riềng",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7a9e",
        "name": "Bến xe Bắc Phan Thiết",
        "address": "Số 2 Từ Văn Tư - TP.Phan Thiết - Tỉnh Bình Thuận",
        "province": "Bình Thuận",
        "district": "Thành phố Phan Thiết",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa1",
        "name": "Bến xe Cà Mau",
        "address": "Quốc lộ 1A - Lý Thường Kiệt - Phường 6 - TP.Cà Mau - Tỉnh Cà Mau",
        "province": "Cà Mau",
        "district": "Thành phố Cà Mau",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405135906_1-ben-xe-ca-mau.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa2",
        "name": "Bến xe Tam Hiệp",
        "address": "358/35B Khóm 2, P.Tam Hiệp - Biên Hòa - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Thành phố Biên Hòa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa3",
        "name": "Bến xe Gia Nghĩa",
        "address": "Đường Mạc Thị Bưởi - TT.Gia Nghĩa - H.Đắk Nông - Tỉnh Đắk Nông",
        "province": "Đắk Nông",
        "district": "Thành phố Gia Nghĩa",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408506542_1-daknong-ben_x20_xe_x20_gia_x20_nghia.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa4",
        "name": "Bến xe Điện Biên Phủ",
        "address": "Trần Đăng Ninh - P.Thanh Bình - TP.Điện Biên Phủ - Tỉnh Điện Biên",
        "province": "Điện Biên",
        "district": "Huyện Điện Biên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408506675_1-dien_x20_bien-ben_x20_xe_x20_dien_x20_bien_x20_phu.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa5",
        "name": "Bến xe Cao Lãnh",
        "address": "Số 71/1 Lý Thường Kiệt - P.2 - TP.Cao Lãnh - Tỉnh Đồng Tháp",
        "province": "Đồng Tháp",
        "district": "Thành phố Cao Lãnh",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa6",
        "name": "Bến xe Ngã Tư Ga",
        "address": "Quốc lộ 1A - P.Thạnh Lộc - Q.12 - TP.Hồ Chí Minh",
        "province": "Hồ Chí Minh",
        "district": "Quận 1",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402286632_1.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa7",
        "name": "Bến xe Hải Dương",
        "address": "Đường Hồng Quang - TP. Hải Dương - Tỉnh Hải Dương",
        "province": "Hải Dương",
        "district": "Thành phố Hải Dương",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405136509_1-hai-duong-ben-xe-khach.png"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa8",
        "name": "Bến xe Cầu Rào",
        "address": "Số 1 Thiên Lôi - Phường Đằng Giang - Ngô Quyền - Hải Phòng",
        "province": "Hải Phòng",
        "district": "Quận Ngô Quyền",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402284731_2.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aaa",
        "name": "Bến xe Tiền Giang",
        "address": "Số 42 Ấp Bắc - Phường 10 - TP Mỹ Tho - Tỉnh Tiền Giang",
        "province": "Tiền Giang",
        "district": "Thành phố Mỹ Tho",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405137490_1-tien-giang-ben_x20_xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aa9",
        "name": "Bến xe Long An",
        "address": "QL1 - P.2 - TP.Tân An - Tỉnh Long An",
        "province": "Long An",
        "district": "Thành phố Tân An",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405137221_1-long_x20_an-ben-xe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aad",
        "name": "Bến xe Sa Thầy",
        "address": "Sa Thầy - Sa Thầy - Kon Tum",
        "province": "Kon Tum",
        "district": "Thành phố Kon Tum",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aab",
        "name": "Bến xe Vĩnh Yên",
        "address": "Phường Khai Quang - TP Vĩnh Yên - Tỉnh Vĩnh Phúc",
        "province": "Vĩnh Phúc",
        "district": "Thành phố Vĩnh Yên",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aac",
        "name": "Bến xe Ngọc Hồi",
        "address": "Ngọc Hồi - Ngọc Hồi - Kon Tum",
        "province": "Kon Tum",
        "district": "Thành phố Kon Tum",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab0",
        "name": "Bến Xe Chợ Lách",
        "address": "Khu phố 1, Thị Trấn Chợ Lách, Huyện Chợ Lách, Tỉnh Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408507431_2-ben_x20_tre-ben_x20_xe_x20_cho_x20_lach.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aae",
        "name": "Bến xe EaKar",
        "address": "Đường Nguyễn Tất Thành, tt Ea kar - Ea Kar - Đắk Lắk",
        "province": "Đắk Lắk",
        "district": "Huyện Ea Kar",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab1",
        "name": "Bến Xe Ba Tri",
        "address": "Tỉnh lộ 885, Thị Trấn Ba Tri - Ba Tri - Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408507925_1-ben_x20_tre-ben_x20_xe_x20_ba_x20_tri.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7aaf",
        "name": "Bến Xe Thảo Châu",
        "address": "Số 122A Nguyễn Thị Định, Phường Phú Tân - TP Bến Tre - Tỉnh Bến Tre",
        "province": "Bến Tre",
        "district": "Thành phố Bến Tre",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab5",
        "name": "Bến xe Yên Nghĩa",
        "address": "QL6 - Yên Nghĩa - Hà Đông - Hà Nội",
        "province": "Hà Nội",
        "district": "Quận Hà Đông",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1402287639_4.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab2",
        "name": "Bến xe Trị An",
        "address": "Vĩnh Cửu - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Huyện Vĩnh Cửu",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1408510194_1-dong_x20_nai-ben_x20_xe_x20_tri_x20_an.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab4",
        "name": "Bến xe Đồng Nai",
        "address": "P. Bình Đa - Biên Hòa - Đồng Nai",
        "province": "Đồng Nai",
        "district": "Thành phố Biên Hòa",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab3",
        "name": "Bến xe Kim Sơn",
        "address": "Thị Trấn Phát Diệm - Kim Sơn - Ninh Bình",
        "province": "Ninh Bình",
        "district": "Thành phố Ninh Bình",
        "image": "http://oto-xemay.vn/Benxe.jpg"
    },
    {
        "_id": "619f58f3e763b3374c7a7ab6",
        "name": "Bến Xe Long Xuyên",
        "address": "Đường Trần Hưng Đạo - Phường Mỹ Phước - TP Long Xuyên - Tỉnh An Giang",
        "province": "An Giang",
        "district": "Thành phố Long Xuyên",
        "image": "http://oto-xemay.vn/images/ben_xe_xe/1405156274_1-long-xuyen-ben-xe.jpg"
    }
]

const generateStation = async() => {
    try {
        let count = 0;
        data.forEach(async(station) => {
            count ++;
            const exitdata = await Station.findOne({_id: station._id, name: station.name}).lean()
            if(!exitdata){
                const newStation = new Station({
                    _id: station._id,
                    name: station.name,
                    address: station.address,
                    province: station.province,
                    district: station.district,
                    image: station.image
                })
                await newStation.save()
            }
        })
        console.log(`Seed ${count} Station Success`);
    } catch (error) {
        throw new Error(error.message);
    }
};
module.exports = {
    generateStation,
    data
}