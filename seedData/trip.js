
const {Seat} = require('../models/Seat')
const Trip = require('../models/Trip')
const {coachData} = require('./coach')

const fs = require('fs')
const Promise = require('bluebird')

const seat16 = [
    "A01","A02","A03","A04","A05","B01","B02","B03","B04","B05","C01","C02","C03","C04","C05","D05"
  ];

const seat29 = [
    "A01","A02","A03","A04","A05","A06","A07","B01","B02","B03","B04","B05","B06","B07","C01","C02","C03","C04","C05","C06", "C07","D01","D02","D03","D04","D05","D06", "D07", "E07"
]

const seat45 = [
    "A01","A02","A03","A04","A05","A06","A07","A08","A09","A10","A11","B01","B02","B03","B04","B05","B06","B07","B08","B09","B10","B11","C01","C02","C03","C04","C05","C06", "C07","C08","C09","C10","C11","D01","D02","D03","D04","D05","D06","D07","D08","D09","D10","D11", "E11"
]

const data = [
    {
        "_id": "61a18f2ad8dd1716a0c1cef7",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Giáp Bát, điểm đến là Bến Xe Liên Tỉnh Đà Lạt vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7933",
        "toStation": "619f58f3e763b3374c7a793d",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1cf28",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Miền Đông, điểm đến là Bến Xe Khách Than Uyên vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7937",
        "toStation": "619f58f3e763b3374c7a7a0f",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1cf59",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lục Ngạn, điểm đến là Bến xe Hưng Yên vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793c",
        "toStation": "619f58f3e763b3374c7a7a84",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1cfec",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Nước Ngầm, điểm đến là Bến xe Đông Triều vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7935",
        "toStation": "619f58f3e763b3374c7a7a07",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d01d",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lương Yên, điểm đến là Bến xe Cửa Lò vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7934",
        "toStation": "619f58f3e763b3374c7a7991",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1cf8a",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Nước Mát, điểm đến là Bến xe Bắc Hà vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793b",
        "toStation": "619f58f3e763b3374c7a794c",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d04e",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Gia Lâm, điểm đến là Bến xe Bờ Sông vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7936",
        "toStation": "619f58f3e763b3374c7a7a24",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d07f",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Đức Linh, điểm đến là Bến xe Đạ Tẻh vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793a",
        "toStation": "619f58f3e763b3374c7a79c2",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1cfbb",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Trung Tâm Đà Nẵng, điểm đến là Bến xe Phan Thiết vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7939",
        "toStation": "619f58f3e763b3374c7a7a7a",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d0b0",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Miền Tây, điểm đến là Bến Xe Quang Vinh vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7938",
        "toStation": "619f58f3e763b3374c7a7a8c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d112",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bình Thuận, điểm đến là Bến xe Lệ Thủy vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7942",
        "toStation": "619f58f3e763b3374c7a794b",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d143",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Liên Tỉnh Đà Lạt, điểm đến là Bến xe Phù Mỹ vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793d",
        "toStation": "619f58f3e763b3374c7a7a99",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d0e1",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Nam Phước, điểm đến là Bến xe Lục Yên vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7940",
        "toStation": "619f58f3e763b3374c7a79f8",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d174",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bãi Cháy, điểm đến là Bến xe Bắc Phan Thiết vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793e",
        "toStation": "619f58f3e763b3374c7a7a9e",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d1d6",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Gành Hào, điểm đến là Bến xe Ngã Tư Vũng Tàu vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7941",
        "toStation": "619f58f3e763b3374c7a7944",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d207",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ngã Tư Vũng Tàu, điểm đến là Bến xe Phú Lộc vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7944",
        "toStation": "619f58f3e763b3374c7a7a91",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d238",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Chùa Non Nước, điểm đến là Bến Xe Tân Kì vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7943",
        "toStation": "619f58f3e763b3374c7a7a0a",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d1a5",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bến Tre, điểm đến là Bến xe Cửa Ông vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a793f",
        "toStation": "619f58f3e763b3374c7a7a19",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d269",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cẩm Xuyên, điểm đến là Bến xe Hà Tĩnh vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7946",
        "toStation": "619f58f3e763b3374c7a7a23",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d2cb",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Nghi Sơn, điểm đến là Bến xe Thăng Bình vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7947",
        "toStation": "619f58f3e763b3374c7a79d9",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d35e",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Chi Lăng, điểm đến là Bến Xe Bãi Cháy vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7949",
        "toStation": "619f58f3e763b3374c7a793e",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d3c0",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lệ Thủy, điểm đến là Bến xe Pleiku vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794b",
        "toStation": "619f58f3e763b3374c7a7a1e",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d422",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Bảo, điểm đến là Bến xe Hội An vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794e",
        "toStation": "619f58f3e763b3374c7a79d7",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d32d",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quỹ Nhất, điểm đến là Bến xe Phú Thọ vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7948",
        "toStation": "619f58f3e763b3374c7a7a96",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d2fc",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tam Quan, điểm đến là Bến xe Tà Lùng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794a",
        "toStation": "619f58f3e763b3374c7a7997",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d29a",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hồng Lĩnh, điểm đến là Bến xe Nguyễn Đáng vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7945",
        "toStation": "619f58f3e763b3374c7a79f3",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d38f",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đô Lương, điểm đến là Bến xe Yên Định vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794d",
        "toStation": "619f58f3e763b3374c7a79d1",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d453",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bến Trại, điểm đến là Bến Xe Chi Lăng vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7951",
        "toStation": "619f58f3e763b3374c7a7949",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d3f1",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bắc Hà, điểm đến là Bến xe Bình Thuận vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794c",
        "toStation": "619f58f3e763b3374c7a7942",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d484",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe khách Vĩnh Thuận, điểm đến là Bến xe Bắc Ruộng vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a794f",
        "toStation": "619f58f3e763b3374c7a7958",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d4e6",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phía Bắc Buôn Ma Thuột, điểm đến là Bến xe Xuân Lộc vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7952",
        "toStation": "619f58f3e763b3374c7a7a33",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d517",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Huệ, điểm đến là Bến xe Thái Thụy vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7956",
        "toStation": "619f58f3e763b3374c7a7a9a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d548",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ea H'leo - Đắk Lắk, điểm đến là Bến xe Bình Phước vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7953",
        "toStation": "619f58f3e763b3374c7a79ae",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d5aa",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Đò Quan, điểm đến là Bến xe Dương Minh Châu vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7955",
        "toStation": "619f58f3e763b3374c7a79e2",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d63d",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bắc Ruộng, điểm đến là Bến Xe Khách Bảo Lộc vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7958",
        "toStation": "619f58f3e763b3374c7a7a8a",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d4b5",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Chiêm Hóa, điểm đến là Bến xe Dầu Giây vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7950",
        "toStation": "619f58f3e763b3374c7a7971",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d66e",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sông Hinh, điểm đến là Bến xe Phù Mỹ vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7959",
        "toStation": "619f58f3e763b3374c7a7a99",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d579",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Cẩm Yên, điểm đến là Bến xe Đông Hà vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7954",
        "toStation": "619f58f3e763b3374c7a7a5b",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d60c",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe La Tiến, điểm đến là Bến xe Nguyễn Đáng vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795a",
        "toStation": "619f58f3e763b3374c7a79f3",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d5db",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Thành Công, điểm đến là Bến xe Trà Vinh vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7957",
        "toStation": "619f58f3e763b3374c7a7a61",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d69f",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trực Ninh, điểm đến là Bến xe Vĩnh Lộc vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795b",
        "toStation": "619f58f3e763b3374c7a79ee",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d701",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Trường Hải, điểm đến là Bến Xe Bình Khánh vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795d",
        "toStation": "619f58f3e763b3374c7a7976",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d732",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Cầu Gồ, điểm đến là Bến xe Hậu Nghĩa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795e",
        "toStation": "619f58f3e763b3374c7a796a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d763",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Diên Khánh, điểm đến là Bến xe Cầu Rào vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795f",
        "toStation": "619f58f3e763b3374c7a7aa8",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d6d0",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ninh Sơn, điểm đến là Bến xe bắc Quảng Ngãi vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a795c",
        "toStation": "619f58f3e763b3374c7a79dc",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d794",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quảng Trị, điểm đến là Bến xe Sa Thầy vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7962",
        "toStation": "619f58f3e763b3374c7a7aad",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d7c5",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hoàn Lão, điểm đến là Bến xe Củ Chi vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7960",
        "toStation": "619f58f3e763b3374c7a79c8",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d7f6",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quế Sơn, điểm đến là Bến xe Buôn Hồ vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7961",
        "toStation": "619f58f3e763b3374c7a7a8b",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d827",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nghĩa Lộ, điểm đến là Đối diện bến xe Sapa vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7966",
        "toStation": "619f58f3e763b3374c7a799c",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d858",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hòa Thành, điểm đến là Bến xe Ea HLeo vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7963",
        "toStation": "619f58f3e763b3374c7a7a97",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d889",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sơn Dương, điểm đến là Bến xe Ngã Tư Vũng Tàu vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7964",
        "toStation": "619f58f3e763b3374c7a7944",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d8ba",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Hiệp Hòa, điểm đến là Bến xe phía bắc TP Huế vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7967",
        "toStation": "619f58f3e763b3374c7a7a60",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d91c",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía bắc Lạng Sơn, điểm đến là Bến xe Mỹ Quới vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7969",
        "toStation": "619f58f3e763b3374c7a7a90",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d94d",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Liên Hương, điểm đến là Bến xe Hải Hậu vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7968",
        "toStation": "619f58f3e763b3374c7a79a3",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d8eb",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mậu A, điểm đến là Bến xe Đông Hưng vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7965",
        "toStation": "619f58f3e763b3374c7a79e7",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d97e",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bình Đại, điểm đến là Bến xe Nguyễn Đáng vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796b",
        "toStation": "619f58f3e763b3374c7a79f3",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d9af",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hậu Nghĩa, điểm đến là Bến Xe Hiệp Hòa vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796a",
        "toStation": "619f58f3e763b3374c7a7967",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1d9e0",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cẩm Khê, điểm đến là Bến xe Phù Cát vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796c",
        "toStation": "619f58f3e763b3374c7a7a1c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1da11",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sầm Sơn, điểm đến là Bến xe Thị Xã Mường Lay vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796d",
        "toStation": "619f58f3e763b3374c7a79b5",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1da42",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khánh Bình, điểm đến là Bến Xe Ba Tri vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7970",
        "toStation": "619f58f3e763b3374c7a7ab1",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1daa4",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kiên Giang, điểm đến là Bến Xe Chợ Lách vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796e",
        "toStation": "619f58f3e763b3374c7a7ab0",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dad5",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Dầu Giây, điểm đến là Bến xe Thường Xuân vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7971",
        "toStation": "619f58f3e763b3374c7a7a8f",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1db06",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Thị Trấn Óc Eo, điểm đến là Bến xe Huệ Thanh vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7974",
        "toStation": "619f58f3e763b3374c7a7a1d",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1da73",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nghĩa Hưng, điểm đến là Bến xe Kiến Xương vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a796f",
        "toStation": "619f58f3e763b3374c7a7a4b",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1db68",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía bắc Nha Trang, điểm đến là Bến xe Hưng Hòa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7978",
        "toStation": "619f58f3e763b3374c7a79fb",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1db37",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Long Khánh, điểm đến là Bến xe Bắc Phan Thiết vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7977",
        "toStation": "619f58f3e763b3374c7a7a9e",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1db99",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cồn Thoi, điểm đến là Bến xe khách Vĩnh Thuận vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7973",
        "toStation": "619f58f3e763b3374c7a794f",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dbca",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sao Đỏ, điểm đến là Bến xe Tam Hiệp vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7972",
        "toStation": "619f58f3e763b3374c7a7aa2",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dc5d",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bồng Tiên, điểm đến là Bến xe phía Nam Thừa Thiên Huế vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7979",
        "toStation": "619f58f3e763b3374c7a7a7d",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dc8e",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Chợ Lục, điểm đến là Bến xe Rạch Sỏi vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797a",
        "toStation": "619f58f3e763b3374c7a79ca",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dbfb",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tịnh Biên, điểm đến là Bến Xe Quang Vinh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7975",
        "toStation": "619f58f3e763b3374c7a7a8c",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dcbf",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hưng Hà, điểm đến là Bến xe Vĩnh Bảo vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797b",
        "toStation": "619f58f3e763b3374c7a794e",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dd21",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe phía Tây Thanh Hóa, điểm đến là Bến Xe Long Xuyên vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797d",
        "toStation": "619f58f3e763b3374c7a7ab6",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dc2c",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bình Khánh, điểm đến là Bến xe Hậu Nghĩa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7976",
        "toStation": "619f58f3e763b3374c7a796a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dcf0",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phú Bình, điểm đến là Bến xe Thanh Ba vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797c",
        "toStation": "619f58f3e763b3374c7a7a75",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dd52",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lâm Hà, điểm đến là Bến xe Thảo Quyên vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7980",
        "toStation": "619f58f3e763b3374c7a7a73",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dd83",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Long Bình, điểm đến là Bến Xe Thảo Châu vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797f",
        "toStation": "619f58f3e763b3374c7a7aaf",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ddb4",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hoài Nhơn, điểm đến là Bến xe Tân Biên vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a797e",
        "toStation": "619f58f3e763b3374c7a79a4",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dde5",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sơn Động, điểm đến là Bến Xe Giáp Bát vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7981",
        "toStation": "619f58f3e763b3374c7a7933",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1de16",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Uông Bí, điểm đến là Bến xe Liên Hương vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7982",
        "toStation": "619f58f3e763b3374c7a7968",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1de47",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Hòn Gai, điểm đến là Bến xe Sa Thầy vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7983",
        "toStation": "619f58f3e763b3374c7a7aad",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dea9",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Phố Mới (Lào Cai), điểm đến là Bến Xe Bình Dương vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7986",
        "toStation": "619f58f3e763b3374c7a7a45",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1de78",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yaun Pa, điểm đến là Bến xe Hộ Phòng vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7984",
        "toStation": "619f58f3e763b3374c7a79a8",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1deda",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Phổ, điểm đến là Bến xe Nông Cống vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7987",
        "toStation": "619f58f3e763b3374c7a79f0",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1df0b",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe huyện Sơn Hòa, điểm đến là Bến Xe An Sương vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7988",
        "toStation": "619f58f3e763b3374c7a7a3a",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1df6d",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hải Hà, điểm đến là Bến xe Đạ Tẻh vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7989",
        "toStation": "619f58f3e763b3374c7a79c2",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1df9e",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nam Sách, điểm đến là Bến xe Đức Long Bảo Lộc vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798a",
        "toStation": "619f58f3e763b3374c7a7a39",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1dfcf",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trung Hà, điểm đến là Bến Xe Liên Tỉnh Đắk Lắk vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798b",
        "toStation": "619f58f3e763b3374c7a7a86",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1df3c",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe khách Đà Nẵng, điểm đến là Bến xe Hộ Phòng vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7985",
        "toStation": "619f58f3e763b3374c7a79a8",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e000",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phía Nam TP Nam Định, điểm đến là Bến Xe Gia Lâm vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798c",
        "toStation": "619f58f3e763b3374c7a7936",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e031",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quỳ Hợp, điểm đến là Bến xe huyện Ba Bể vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798f",
        "toStation": "619f58f3e763b3374c7a79ad",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e062",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tiên Kỳ, điểm đến là Bến Xe Yên Châu vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798d",
        "toStation": "619f58f3e763b3374c7a7a04",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e126",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Bình An, điểm đến là Bến xe Cao Lãnh vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7992",
        "toStation": "619f58f3e763b3374c7a7aa5",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e093",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hoàng Mai, điểm đến là Bến Xe Liên Tỉnh Đắk Lắk vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a798e",
        "toStation": "619f58f3e763b3374c7a7a86",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e0c4",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Liên Tỉnh Quảng Trị, điểm đến là Bến xe Phúc Yên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7990",
        "toStation": "619f58f3e763b3374c7a79f7",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e0f5",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cửa Lò, điểm đến là Bến Xe Đức Linh vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7991",
        "toStation": "619f58f3e763b3374c7a793a",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e157",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quỳnh Nhai, điểm đến là Bến xe Phúc Yên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7993",
        "toStation": "619f58f3e763b3374c7a79f7",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e1b9",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tà Lùng, điểm đến là Bến xe Thường Xuân vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7997",
        "toStation": "619f58f3e763b3374c7a7a8f",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e1ea",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe khách Thượng Lý, điểm đến là Bến xe Vinh vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799a",
        "toStation": "619f58f3e763b3374c7a7a58",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e21b",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Con Cuông, điểm đến là Bến xe Thạnh Phú vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799b",
        "toStation": "619f58f3e763b3374c7a7a22",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e24c",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Đối diện bến xe Sapa, điểm đến là Bến xe Ba Hòn vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799d",
        "toStation": "619f58f3e763b3374c7a7a82",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e188",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phương Trang, điểm đến là Bến xe Bắc Ninh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7994",
        "toStation": "619f58f3e763b3374c7a7a44",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e27d",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Chơn Thành, điểm đến là Bến Xe Khách Than Uyên vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7998",
        "toStation": "619f58f3e763b3374c7a7a0f",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e2ae",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yên Lạc, điểm đến là Bến xe Mường Chà vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7995",
        "toStation": "619f58f3e763b3374c7a79b3",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e2df",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Phố Lu, điểm đến là Bến Xe Cẩm Yên vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799e",
        "toStation": "619f58f3e763b3374c7a7954",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e341",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Yên Lập, điểm đến là Bến Xe Như Thanh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799f",
        "toStation": "619f58f3e763b3374c7a7a0d",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e372",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kinh Môn, điểm đến là Bến xe Cái Nước vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a1",
        "toStation": "619f58f3e763b3374c7a79b1",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e3a3",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Đối diện bến xe Sapa, điểm đến là Bến xe Thuận An vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a799c",
        "toStation": "619f58f3e763b3374c7a79f2",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e3d4",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Sơn Tây, điểm đến là Bến xe Cà Mau vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7996",
        "toStation": "619f58f3e763b3374c7a7aa1",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e310",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sô Tô, điểm đến là Bến xe Ayun Pa vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a0",
        "toStation": "619f58f3e763b3374c7a7a66",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e405",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía Đông Sao Đỏ, điểm đến là Bến xe Đại Từ vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7999",
        "toStation": "619f58f3e763b3374c7a79ea",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e467",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hộ Phòng, điểm đến là Bến xe Cao Bằng vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a8",
        "toStation": "619f58f3e763b3374c7a7aa0",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e436",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bảo Bình, điểm đến là Bến xe An Khê vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a7",
        "toStation": "619f58f3e763b3374c7a7a87",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e498",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tây Sơn - Hà Tĩnh, điểm đến là Bến xe Thái Nguyên vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ab",
        "toStation": "619f58f3e763b3374c7a7a5f",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e4fa",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hải Hậu, điểm đến là Bến xe Kiên Giang vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a3",
        "toStation": "619f58f3e763b3374c7a796e",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e52b",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tân Biên, điểm đến là Bến xe Cửa Ông vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a4",
        "toStation": "619f58f3e763b3374c7a7a19",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e55c",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vân Đồn, điểm đến là Bến xe Nam Hải vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a5",
        "toStation": "619f58f3e763b3374c7a7a8d",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e58d",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tân Hồng, điểm đến là Bến Xe Tân Uyên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a6",
        "toStation": "619f58f3e763b3374c7a7a4d",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e5be",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quất Lâm, điểm đến là Bến Xe Khách Hòn Gai vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a9",
        "toStation": "619f58f3e763b3374c7a7983",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e620",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phước Long (Bạc Liêu), điểm đến là Bến xe Văn Bàn vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ac",
        "toStation": "619f58f3e763b3374c7a79ce",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e4c9",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ninh Giang, điểm đến là Bến xe Sa Pa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79a2",
        "toStation": "619f58f3e763b3374c7a7a27",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e7d9",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thị Xã Mường Lay, điểm đến là Bến xe Hương Khê vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b5",
        "toStation": "619f58f3e763b3374c7a79bc",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e682",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe huyện Ba Bể, điểm đến là Bến xe Nghĩa Đàn vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ad",
        "toStation": "619f58f3e763b3374c7a7a32",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e5ef",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Thanh Sơn, điểm đến là Bến xe Sơn Động vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79aa",
        "toStation": "619f58f3e763b3374c7a7981",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e777",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mường Chà, điểm đến là Bến xe Bù Gia Mập vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b3",
        "toStation": "619f58f3e763b3374c7a7a68",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e746",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cái Nước, điểm đến là Bến xe Ba Hòn vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b1",
        "toStation": "619f58f3e763b3374c7a7a82",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e715",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thị Xã La Gi, điểm đến là Bến Xe Tam Quan vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b2",
        "toStation": "619f58f3e763b3374c7a794a",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e6e4",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tánh Linh, điểm đến là Bến xe Bờ Sông vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b0",
        "toStation": "619f58f3e763b3374c7a7a24",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e6b3",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phan Rí Cửa, điểm đến là Bến xe Nghĩa Lộ vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79af",
        "toStation": "619f58f3e763b3374c7a7966",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e651",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bình Phước, điểm đến là Bến xe Ea HLeo vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ae",
        "toStation": "619f58f3e763b3374c7a7a97",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e7a8",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bản Phủ, điểm đến là Bến xe Phía Nam TP Nam Định vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b4",
        "toStation": "619f58f3e763b3374c7a798c",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e83b",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sa Đéc, điểm đến là Bến Xe phía Tây Thanh Hóa vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b7",
        "toStation": "619f58f3e763b3374c7a797d",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e86c",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Châu Thành, điểm đến là Bến Xe Châu Đốc vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b8",
        "toStation": "619f58f3e763b3374c7a7a76",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e89d",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Trụ, điểm đến là Bến Xe Yên Lập vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ba",
        "toStation": "619f58f3e763b3374c7a799f",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e8ce",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hương Khê, điểm đến là Bến xe Cao Bằng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79bc",
        "toStation": "619f58f3e763b3374c7a7aa0",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e80a",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ghi Sê, điểm đến là Bến xe Nông Cống vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b6",
        "toStation": "619f58f3e763b3374c7a79f0",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e992",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kỳ Anh, điểm đến là Bến xe TP Vĩnh Long vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79be",
        "toStation": "619f58f3e763b3374c7a79f4",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e9c3",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hải Tân, điểm đến là Bến Xe Chơn Thành vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79bf",
        "toStation": "619f58f3e763b3374c7a7998",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e8ff",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yên Minh, điểm đến là Bến xe Quế Sơn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79b9",
        "toStation": "619f58f3e763b3374c7a7961",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e930",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Đan Phượng, điểm đến là Bến Xe Trân Bảo Trân vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79bb",
        "toStation": "619f58f3e763b3374c7a7a12",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e961",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Thọ, điểm đến là Bến xe thị trấn Hai Riêng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79bd",
        "toStation": "619f58f3e763b3374c7a79d4",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1e9f4",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lạc Long, điểm đến là Bến Xe Thanh Sơn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c0",
        "toStation": "619f58f3e763b3374c7a79aa",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ea56",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đạ Tẻh, điểm đến là Bến Xe Phú Tân vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c2",
        "toStation": "619f58f3e763b3374c7a7a36",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ea87",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ea Súp, điểm đến là Bến xe phía Tây vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c3",
        "toStation": "619f58f3e763b3374c7a79eb",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eab8",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kinh Cùng, điểm đến là Bến xe Vạn Giã vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c4",
        "toStation": "619f58f3e763b3374c7a7a83",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ea25",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lạch Tray, điểm đến là Bến xe Cầu Rào vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c1",
        "toStation": "619f58f3e763b3374c7a7aa8",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eae9",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Long Mỹ, điểm đến là Bến xe Sao Đỏ vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c5",
        "toStation": "619f58f3e763b3374c7a7972",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eb1a",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cái Tắc, điểm đến là Bến xe Tam Kỳ vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c6",
        "toStation": "619f58f3e763b3374c7a7a5a",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ebde",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Rạch Sỏi, điểm đến là Bến xe Văn Bàn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ca",
        "toStation": "619f58f3e763b3374c7a79ce",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eb4b",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quận 8, điểm đến là Bến Xe Khách Hòn Gai vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c7",
        "toStation": "619f58f3e763b3374c7a7983",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eb7c",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Củ Chi, điểm đến là Bến xe Phía Bắc Buôn Ma Thuột vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c8",
        "toStation": "619f58f3e763b3374c7a7952",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ebad",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ân Thi, điểm đến là Bến xe Yên Lạc vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79c9",
        "toStation": "619f58f3e763b3374c7a7995",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ec0f",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe An Minh, điểm đến là Bến xe Gia Lai vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79cb",
        "toStation": "619f58f3e763b3374c7a7a46",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ec71",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hữu Lũng, điểm đến là Bến xe Kỳ Anh vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79cd",
        "toStation": "619f58f3e763b3374c7a79be",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eca2",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Văn Bàn, điểm đến là Bến xe Ea Súp vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ce",
        "toStation": "619f58f3e763b3374c7a79c3",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ecd3",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Trọng, điểm đến là Bến xe Chiêm Hóa vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79cf",
        "toStation": "619f58f3e763b3374c7a7950",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ec40",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ninh Hòa, điểm đến là Bến Xe Quỳnh Côi vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79cc",
        "toStation": "619f58f3e763b3374c7a7a2e",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ed04",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Di Linh, điểm đến là Bến xe Móng Cái vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d0",
        "toStation": "619f58f3e763b3374c7a7a4c",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ed35",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yên Định, điểm đến là Bến xe Tà Lùng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d1",
        "toStation": "619f58f3e763b3374c7a7997",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ed66",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe thị trấn Hai Riêng, điểm đến là Bến Xe Cẩm Yên vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d4",
        "toStation": "619f58f3e763b3374c7a7954",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ed97",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe thị xã Cửa Lò, điểm đến là Bến xe Huệ Thanh vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d2",
        "toStation": "619f58f3e763b3374c7a7a1d",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1edf9",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Hạ Hòa, điểm đến là Bến xe TP Tuyên Quang vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d3",
        "toStation": "619f58f3e763b3374c7a7a7e",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ee5b",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe bắc Quảng Ngãi, điểm đến là Bến Xe Gò Bồi vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79dc",
        "toStation": "619f58f3e763b3374c7a7a6c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ee2a",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quy Đạt, điểm đến là Bến xe Cầu Rào vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d6",
        "toStation": "619f58f3e763b3374c7a7aa8",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1edc8",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe La Hai, điểm đến là Bến xe Tân Biên vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d5",
        "toStation": "619f58f3e763b3374c7a79a4",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ee8c",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hội An, điểm đến là Bến xe Bắc Hà vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d7",
        "toStation": "619f58f3e763b3374c7a794c",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eebd",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thăng Bình, điểm đến là Bến xe Lào Cai vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d9",
        "toStation": "619f58f3e763b3374c7a7a56",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1eeee",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tiên Phước, điểm đến là Bến Xe Lương Yên vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79d8",
        "toStation": "619f58f3e763b3374c7a7934",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ef1f",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hòn Gai, điểm đến là Bến Xe Nước Mát vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79da",
        "toStation": "619f58f3e763b3374c7a793b",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ef50",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mông Dương, điểm đến là Bến Xe Tịnh Biên vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79db",
        "toStation": "619f58f3e763b3374c7a7975",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1efe3",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía bắc Đông Hà, điểm đến là Bến xe Bến Trại vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79dd",
        "toStation": "619f58f3e763b3374c7a7951",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f014",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hải Lăng, điểm đến là Đối diện bến xe Sapa vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79df",
        "toStation": "619f58f3e763b3374c7a799d",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ef81",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Khe Sanh, điểm đến là Bến xe Niệm Nghĩa vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79de",
        "toStation": "619f58f3e763b3374c7a7a3b",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f076",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đông Hưng, điểm đến là Bến Xe Chợ Mới vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e7",
        "toStation": "619f58f3e763b3374c7a7a25",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1efb2",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kế Sách, điểm đến là Bến xe Vân Đồn vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e0",
        "toStation": "619f58f3e763b3374c7a79a5",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f045",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trà Men, điểm đến là Bến xe Ea H'leo - Đắk Lắk vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e1",
        "toStation": "619f58f3e763b3374c7a7953",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f0a7",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Dương Minh Châu, điểm đến là Bến xe Krông Pa vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e2",
        "toStation": "619f58f3e763b3374c7a7a67",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f109",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tiền Hải, điểm đến là Bến xe Lạng Sơn vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e8",
        "toStation": "619f58f3e763b3374c7a7a7b",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f0d8",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Châu Thành (Tây Ninh), điểm đến là Bến xe huyện Đắk Đoa vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e3",
        "toStation": "619f58f3e763b3374c7a7a89",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f13a",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nà Hang, điểm đến là Bến Xe Ngọc Lặc vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e5",
        "toStation": "619f58f3e763b3374c7a7a05",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f16b",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Gò Dầu, điểm đến là Bến xe tỉnh Lai Châu vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e4",
        "toStation": "619f58f3e763b3374c7a7a53",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f19c",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hàm Yên, điểm đến là Bến xe Mường La vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e6",
        "toStation": "619f58f3e763b3374c7a7a6a",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f1fe",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đại Từ, điểm đến là Bến xe Ngã Tư Vũng Tàu vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ea",
        "toStation": "619f58f3e763b3374c7a7944",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f1cd",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phổ Yên, điểm đến là Bến xe Ea Súp vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79e9",
        "toStation": "619f58f3e763b3374c7a79c3",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f22f",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía Tây, điểm đến là Bến xe Phú Lộc vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79eb",
        "toStation": "619f58f3e763b3374c7a7a91",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f2c2",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cẩm Thủy, điểm đến là Bến xe Phúc Yên vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ed",
        "toStation": "619f58f3e763b3374c7a79f7",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f260",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hoằng Hóa, điểm đến là Bến xe Gành Hào vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ec",
        "toStation": "619f58f3e763b3374c7a7941",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f2f3",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Lộc, điểm đến là Bến xe Ninh Giang vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ee",
        "toStation": "619f58f3e763b3374c7a79a2",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f324",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nông Cống, điểm đến là Bến xe tỉnh Lai Châu vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f0",
        "toStation": "619f58f3e763b3374c7a7a53",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f291",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nga Sơn, điểm đến là Bến Xe Phố Lu vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ef",
        "toStation": "619f58f3e763b3374c7a799e",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f355",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe TP Huế, điểm đến là Bến xe Sông Hinh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f1",
        "toStation": "619f58f3e763b3374c7a7959",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f386",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thuận An, điểm đến là Bến xe Hải Tân vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f2",
        "toStation": "619f58f3e763b3374c7a79bf",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f3b7",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nguyễn Đáng, điểm đến là Bến xe Đức Long Bảo Lộc vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f3",
        "toStation": "619f58f3e763b3374c7a7a39",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f3e8",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vũng Liêm, điểm đến là Bến xe Sông Cầu vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f5",
        "toStation": "619f58f3e763b3374c7a7a38",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f44a",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lập Thạch, điểm đến là Bến xe Chùa Non Nước vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f6",
        "toStation": "619f58f3e763b3374c7a7943",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f419",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe TP Vĩnh Long, điểm đến là Bến xe Hộ Phòng vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f4",
        "toStation": "619f58f3e763b3374c7a79a8",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f47b",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phúc Yên, điểm đến là Bến Xe Long Bình vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f7",
        "toStation": "619f58f3e763b3374c7a797f",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f4dd",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Gia Bình, điểm đến là Bến xe tỉnh Lai Châu vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f9",
        "toStation": "619f58f3e763b3374c7a7a53",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f50e",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Huyện Bắc Quang, điểm đến là Bến xe Quỳnh Nhai vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79fa",
        "toStation": "619f58f3e763b3374c7a7993",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f53f",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hưng Hòa, điểm đến là Bến xe Sao Đỏ vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79fb",
        "toStation": "619f58f3e763b3374c7a7972",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f570",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Diêm Điền 1, điểm đến là Bến xe Phía Nam Buôn Ma Thuột vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79fc",
        "toStation": "619f58f3e763b3374c7a7a7f",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f4ac",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lục Yên, điểm đến là Bến Xe Khánh Bình vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79f8",
        "toStation": "619f58f3e763b3374c7a7970",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f5a1",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Yên Thành, điểm đến là Bến xe Mường La vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a06",
        "toStation": "619f58f3e763b3374c7a7a6a",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f5d2",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Ngọc Lặc, điểm đến là Bến Xe Khách Huyện Bắc Quang vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a05",
        "toStation": "619f58f3e763b3374c7a79fa",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f603",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe khách huyện Tuần Giáo, điểm đến là Bến xe khách Ô Môn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a01",
        "toStation": "619f58f3e763b3374c7a7a2a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f634",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Sim, điểm đến là Bến xe Mường La vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a00",
        "toStation": "619f58f3e763b3374c7a7a6a",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f696",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Minh Lộc, điểm đến là Bến xe Tân Phú vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79fe",
        "toStation": "619f58f3e763b3374c7a7a71",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f6c7",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Yên Châu, điểm đến là Bến xe Hải Hậu vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a04",
        "toStation": "619f58f3e763b3374c7a79a3",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f665",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Quảng Xương, điểm đến là Bến xe Bắc Hà vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79fd",
        "toStation": "619f58f3e763b3374c7a794c",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f6f8",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tân Hà, điểm đến là Bến xe TP Tuyên Quang vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a02",
        "toStation": "619f58f3e763b3374c7a7a7e",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f729",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đoan Hùng, điểm đến là Bến xe Phía Bắc Buôn Ma Thuột vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a79ff",
        "toStation": "619f58f3e763b3374c7a7952",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f75a",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tam Điệp, điểm đến là Bến xe Quảng Ngãi vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a03",
        "toStation": "619f58f3e763b3374c7a7a64",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f7bc",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bắc Vinh, điểm đến là Bến Xe Tân Uyên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0c",
        "toStation": "619f58f3e763b3374c7a7a4d",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f78b",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Yên Mỹ, điểm đến là Bến xe Ninh Hòa vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0e",
        "toStation": "619f58f3e763b3374c7a79cc",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f81e",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Pác Nặm, điểm đến là Bến xe Lệ Thủy vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a08",
        "toStation": "619f58f3e763b3374c7a794b",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f7ed",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thạnh Trị, điểm đến là Bến xe La Tiến vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a10",
        "toStation": "619f58f3e763b3374c7a795a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f880",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tân Kì, điểm đến là Bến xe Cẩm Khê vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0a",
        "toStation": "619f58f3e763b3374c7a796c",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f8e2",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lý Nhân, điểm đến là Bến xe Sóc Trăng vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0b",
        "toStation": "619f58f3e763b3374c7a7a5c",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f913",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Than Uyên, điểm đến là Bến xe Thị Xã Mường Lay vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0f",
        "toStation": "619f58f3e763b3374c7a79b5",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f944",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Như Thanh, điểm đến là Bến xe Ninh Sơn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a0d",
        "toStation": "619f58f3e763b3374c7a795c",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f975",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thanh Chương, điểm đến là Bến Xe Gò Bồi vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a11",
        "toStation": "619f58f3e763b3374c7a7a6c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f84f",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đông Triều, điểm đến là Bến xe Móng Cái vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a07",
        "toStation": "619f58f3e763b3374c7a7a4c",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f9d7",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Krông Bông, điểm đến là Bến xe Sông Ray vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a13",
        "toStation": "619f58f3e763b3374c7a7a78",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fafd",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Giao Thủy, điểm đến là Bến xe Ninh Giang vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1a",
        "toStation": "619f58f3e763b3374c7a79a2",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1facc",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trung tâm Thị Xã Cẩm Phả, điểm đến là Bến xe Lào Cai vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a15",
        "toStation": "619f58f3e763b3374c7a7a56",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f9a6",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Trân Bảo Trân, điểm đến là Bến Xe Hiệp Hòa vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a12",
        "toStation": "619f58f3e763b3374c7a7967",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fa39",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cửa Ông, điểm đến là Bến xe Cồn Thoi vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a19",
        "toStation": "619f58f3e763b3374c7a7973",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fa6a",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hồng Ngự, điểm đến là Bến xe Vạn Giã vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a16",
        "toStation": "619f58f3e763b3374c7a7a83",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fb2e",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Chợ Lớn, điểm đến là Bến xe Ghi Sê vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a18",
        "toStation": "619f58f3e763b3374c7a79b6",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fa08",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Krông Năng, điểm đến là Bến xe Thái Thụy vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a14",
        "toStation": "619f58f3e763b3374c7a7a9a",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fa9b",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nho Quan, điểm đến là Bến Xe Ngọc Lặc vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a17",
        "toStation": "619f58f3e763b3374c7a7a05",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1f8b1",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Tỉnh Ninh Thuận, điểm đến là Bến xe Đoan Hùng vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a09",
        "toStation": "619f58f3e763b3374c7a79ff",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fb90",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phù Cát, điểm đến là Bến xe Ayun Pa vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1c",
        "toStation": "619f58f3e763b3374c7a7a66",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fbc1",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Huệ Thanh, điểm đến là Bến xe Đức Long Bảo Lộc vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1d",
        "toStation": "619f58f3e763b3374c7a7a39",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fb5f",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tri Tôn, điểm đến là Bến xe Gò Dầu vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1b",
        "toStation": "619f58f3e763b3374c7a79e4",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fbf2",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hùng Vương (Cần Thơ), điểm đến là Bến Xe Quy Nhơn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1f",
        "toStation": "619f58f3e763b3374c7a7a85",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fc54",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đồng Quang, điểm đến là Bến Xe Trung Tâm Đà Nẵng vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a20",
        "toStation": "619f58f3e763b3374c7a7939",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fce7",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thạnh Phú, điểm đến là Bến xe Đông Hưng vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a22",
        "toStation": "619f58f3e763b3374c7a79e7",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fd18",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bờ Sông, điểm đến là Bến Xe Quỳnh Côi vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a24",
        "toStation": "619f58f3e763b3374c7a7a2e",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fc23",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Pleiku, điểm đến là Bến xe Ân Thi vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a1e",
        "toStation": "619f58f3e763b3374c7a79c9",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fc85",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tân Châu, điểm đến là Bến Xe Bình Dương vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a21",
        "toStation": "619f58f3e763b3374c7a7a45",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fcb6",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hà Tĩnh, điểm đến là Bến xe Lạng Sơn vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a23",
        "toStation": "619f58f3e763b3374c7a7a7b",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fd49",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Chợ Mới, điểm đến là Bến xe Cần Thơ vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a25",
        "toStation": "619f58f3e763b3374c7a7a77",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fdab",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hữu  nghị Quan, điểm đến là Bến xe Hưng Hòa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a26",
        "toStation": "619f58f3e763b3374c7a79fb",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fddc",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe khách thị trấn Thọ Xuân, điểm đến là Bến xe Cửa Lò vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2f",
        "toStation": "619f58f3e763b3374c7a7991",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fd7a",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sa Pa, điểm đến là Bến xe Tân Phú vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a27",
        "toStation": "619f58f3e763b3374c7a7a71",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fe0d",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tiên Yên, điểm đến là Bến xe Cao Lãnh vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a28",
        "toStation": "619f58f3e763b3374c7a7aa5",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fe3e",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Cờ Đỏ, điểm đến là Bến Xe Thanh Sơn vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a29",
        "toStation": "619f58f3e763b3374c7a79aa",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fed1",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Rạng Đông, điểm đến là Bến xe Pleiku vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2c",
        "toStation": "619f58f3e763b3374c7a7a1e",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fe6f",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe khách Ô Môn, điểm đến là Bến Xe Khách Bảo Lộc vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2a",
        "toStation": "619f58f3e763b3374c7a7a8a",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fea0",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bến Cát, điểm đến là Bến xe Nông Cống vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a30",
        "toStation": "619f58f3e763b3374c7a79f0",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ff02",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bình Định, điểm đến là Bến xe Kon Tum vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2b",
        "toStation": "619f58f3e763b3374c7a7a55",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ff33",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Phía Nam Buôn Mê Thuột, điểm đến là Bến xe Tiền Giang vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2d",
        "toStation": "619f58f3e763b3374c7a7aaa",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ff95",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Quỳnh Côi, điểm đến là Bến xe Diêm Điền 1 vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a2e",
        "toStation": "619f58f3e763b3374c7a79fc",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ffc6",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nghĩa Đàn, điểm đến là Bến xe Thành Thắng vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a32",
        "toStation": "619f58f3e763b3374c7a7a51",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1fff7",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sông Cầu, điểm đến là Bến xe Bắc Ruộng vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a38",
        "toStation": "619f58f3e763b3374c7a7958",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20028",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe An Sương, điểm đến là Bến xe Ngã Tư Ga vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3a",
        "toStation": "619f58f3e763b3374c7a7aa6",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c1ff64",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lam Hồng, điểm đến là Bến xe thị xã Cửa Lò vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a31",
        "toStation": "619f58f3e763b3374c7a79d2",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20059",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Xuân Lộc, điểm đến là Bến xe Pleiku vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a33",
        "toStation": "619f58f3e763b3374c7a7a1e",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2008a",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Cam Ranh, điểm đến là Bến xe Bắc Ruộng vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a34",
        "toStation": "619f58f3e763b3374c7a7958",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c200bb",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hương Sơn, điểm đến là Bến xe Trà Vinh vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a35",
        "toStation": "619f58f3e763b3374c7a7a61",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2014e",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Long Bảo Lộc, điểm đến là Bến xe Sơn La vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a39",
        "toStation": "619f58f3e763b3374c7a7a3e",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2017f",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Niệm Nghĩa, điểm đến là Bến xe Nghĩa Hưng vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3b",
        "toStation": "619f58f3e763b3374c7a796f",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c200ec",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Phú Tân, điểm đến là Bến xe Văn Bàn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a36",
        "toStation": "619f58f3e763b3374c7a79ce",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c201e1",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đức Long Gia Lai, điểm đến là Bến xe Hải Lăng vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3d",
        "toStation": "619f58f3e763b3374c7a79df",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20212",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hà Tiên, điểm đến là Bến Xe Tri Tôn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3f",
        "toStation": "619f58f3e763b3374c7a7a1b",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2011d",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Triều Dương, điểm đến là Bến xe Văn Bàn vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a37",
        "toStation": "619f58f3e763b3374c7a79ce",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c201b0",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Gò Công, điểm đến là Bến Xe Bến Tre vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3c",
        "toStation": "619f58f3e763b3374c7a793f",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20243",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sơn La, điểm đến là Bến xe Quỳnh Nhai vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a3e",
        "toStation": "619f58f3e763b3374c7a7993",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20274",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tây Ninh, điểm đến là Bến xe Nga Sơn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a41",
        "toStation": "619f58f3e763b3374c7a79ef",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c202a5",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Việt Trì, điểm đến là Bến xe Cao Bằng vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a40",
        "toStation": "619f58f3e763b3374c7a7aa0",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20307",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bắc Ninh, điểm đến là Bến xe Lệ Thủy vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a44",
        "toStation": "619f58f3e763b3374c7a794b",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20369",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bình Dương, điểm đến là Bến xe Ea HLeo vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a45",
        "toStation": "619f58f3e763b3374c7a7a97",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c202d6",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bắc Kạn, điểm đến là Bến xe Phú Lộc vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a43",
        "toStation": "619f58f3e763b3374c7a7a91",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c203cb",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe TX Phủ Lý, điểm đến là Bến Xe Long Xuyên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a47",
        "toStation": "619f58f3e763b3374c7a7ab6",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20338",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bạc Liêu, điểm đến là Bến xe 91B Cần Thơ vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a42",
        "toStation": "619f58f3e763b3374c7a7a4a",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2039a",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Gia Lai, điểm đến là Bến xe Trị An vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a46",
        "toStation": "619f58f3e763b3374c7a7ab2",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2042d",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe KBang, điểm đến là Bến xe Kỳ Anh vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a49",
        "toStation": "619f58f3e763b3374c7a79be",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c203fc",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vị Thanh, điểm đến là Bến xe Vĩnh Lộc vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a48",
        "toStation": "619f58f3e763b3374c7a79ee",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2045e",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe 91B Cần Thơ, điểm đến là Bến xe Thảo Quyên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4a",
        "toStation": "619f58f3e763b3374c7a7a73",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2048f",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kiến Xương, điểm đến là Bến Xe Khách Tỉnh Ninh Thuận vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4b",
        "toStation": "619f58f3e763b3374c7a7a09",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c204c0",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Móng Cái, điểm đến là Bến xe Ghi Sê vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4c",
        "toStation": "619f58f3e763b3374c7a79b6",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c204f1",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tân Uyên, điểm đến là Bến Xe Trân Bảo Trân vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4d",
        "toStation": "619f58f3e763b3374c7a7a12",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20584",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tây Sơn, điểm đến là Bến xe Quỳ Hợp vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4f",
        "toStation": "619f58f3e763b3374c7a798f",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c205e6",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bắc Giang, điểm đến là Bến xe phía bắc Thanh Hóa vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a52",
        "toStation": "619f58f3e763b3374c7a7a5d",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20522",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Phước Long (Bình Phước), điểm đến là Bến xe Hữu  nghị Quan vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a4e",
        "toStation": "619f58f3e763b3374c7a7a26",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20553",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Buôn Mê Thuột, điểm đến là Bến xe huyện Ba Bể vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a50",
        "toStation": "619f58f3e763b3374c7a79ad",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c205b5",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thành Thắng, điểm đến là Bến xe Quỹ Nhất vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a51",
        "toStation": "619f58f3e763b3374c7a7948",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20648",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Rạch Giá, điểm đến là Bến xe Ngọc Hồi vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a54",
        "toStation": "619f58f3e763b3374c7a7aac",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20679",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe tỉnh Lai Châu, điểm đến là Bến xe phía Nam Thừa Thiên Huế vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a53",
        "toStation": "619f58f3e763b3374c7a7a7d",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20617",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kon Tum, điểm đến là Bến Xe Tây Sơn - Hà Tĩnh vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a55",
        "toStation": "619f58f3e763b3374c7a79ab",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c206aa",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ninh Bình, điểm đến là Bến Xe Trân Bảo Trân vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a59",
        "toStation": "619f58f3e763b3374c7a7a12",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c206db",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lào Cai, điểm đến là Bến xe Diêm Điền 1 vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a56",
        "toStation": "619f58f3e763b3374c7a79fc",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2070c",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nam Định, điểm đến là Bến xe Ghi Sê vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a57",
        "toStation": "619f58f3e763b3374c7a79b6",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2073d",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vinh, điểm đến là Bến xe Quế Sơn vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a58",
        "toStation": "619f58f3e763b3374c7a7961",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2076e",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tam Kỳ, điểm đến là Bến xe Tà Lùng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5a",
        "toStation": "619f58f3e763b3374c7a7997",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2079f",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đông Hà, điểm đến là Bến Xe Long Bình vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5b",
        "toStation": "619f58f3e763b3374c7a797f",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20801",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía bắc TP Huế, điểm đến là Bến xe Rạch Giá vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a60",
        "toStation": "619f58f3e763b3374c7a7a54",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c207d0",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sóc Trăng, điểm đến là Bến Xe Liên Tỉnh Đà Lạt vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5c",
        "toStation": "619f58f3e763b3374c7a793d",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20832",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía bắc Thanh Hóa, điểm đến là Bến xe Nguyễn Đáng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5d",
        "toStation": "619f58f3e763b3374c7a79f3",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20863",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thái Nguyên, điểm đến là Bến xe Hải Hậu vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5f",
        "toStation": "619f58f3e763b3374c7a79a3",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c208c5",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trà Vinh, điểm đến là Bến Xe Thành Công vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a61",
        "toStation": "619f58f3e763b3374c7a7957",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c208f6",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ba Đồn, điểm đến là Bến xe Mũi Né vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6b",
        "toStation": "619f58f3e763b3374c7a7a9b",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20927",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Tam Nông, điểm đến là Bến xe Ngã Tư Vũng Tàu vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a65",
        "toStation": "619f58f3e763b3374c7a7944",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20894",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía nam Thanh Hóa, điểm đến là Bến xe Cầu Rào vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a5e",
        "toStation": "619f58f3e763b3374c7a7aa8",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20958",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quảng Ngãi, điểm đến là Bến xe Vinh vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a64",
        "toStation": "619f58f3e763b3374c7a7a58",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c209ba",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Krông Pa, điểm đến là Bến xe Hà Tĩnh vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a67",
        "toStation": "619f58f3e763b3374c7a7a23",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20a1c",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mường La, điểm đến là Bến xe Sa Pa vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6a",
        "toStation": "619f58f3e763b3374c7a7a27",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20a4d",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Biên Hòa, điểm đến là Bến xe Quỳnh Nhai vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a69",
        "toStation": "619f58f3e763b3374c7a7993",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20989",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ayun Pa, điểm đến là Bến xe Quỳnh Nhai vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a66",
        "toStation": "619f58f3e763b3374c7a7993",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20a7e",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Long, điểm đến là Bến xe Long An vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a62",
        "toStation": "619f58f3e763b3374c7a7aa9",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20aaf",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yên Bái, điểm đến là Bến xe Thái Nguyên vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a63",
        "toStation": "619f58f3e763b3374c7a7a5f",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c209eb",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bù Gia Mập, điểm đến là Bến Xe Nước Mát vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a68",
        "toStation": "619f58f3e763b3374c7a793b",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20ae0",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe thị xã Tây Ninh, điểm đến là Bến xe Bình Thuận vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a72",
        "toStation": "619f58f3e763b3374c7a7942",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20b73",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Thái Bình, điểm đến là Bến xe KBang vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6d",
        "toStation": "619f58f3e763b3374c7a7a49",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20b11",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thanh Ba, điểm đến là Bến xe Bình Thuận vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a75",
        "toStation": "619f58f3e763b3374c7a7942",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20b42",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thảo Quyên, điểm đến là Bến Xe Bình Khánh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a73",
        "toStation": "619f58f3e763b3374c7a7976",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20ba4",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Gò Bồi, điểm đến là Bến xe Cao Bằng vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6c",
        "toStation": "619f58f3e763b3374c7a7aa0",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20bd5",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tân Thanh, điểm đến là Bến Xe Quang Vinh vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6e",
        "toStation": "619f58f3e763b3374c7a7a8c",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20c37",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Bồng Sơn, điểm đến là Bến xe Hải Lăng vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a6f",
        "toStation": "619f58f3e763b3374c7a79df",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20c99",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Quảng Trường Gia, điểm đến là Bến xe Minh Lộc vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a74",
        "toStation": "619f58f3e763b3374c7a79fe",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20c06",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tân Phú, điểm đến là Bến Xe Lộc Ninh vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a71",
        "toStation": "619f58f3e763b3374c7a7a94",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20cca",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Châu Đốc, điểm đến là Bến Xe Phước Long (Bình Phước) vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a76",
        "toStation": "619f58f3e763b3374c7a7a4e",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20d2c",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sông Ray, điểm đến là Bến xe Krông Pa vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a78",
        "toStation": "619f58f3e763b3374c7a7a67",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20d5d",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Châu, điểm đến là Bến Xe Phố Lu vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a79",
        "toStation": "619f58f3e763b3374c7a799e",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20cfb",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cần Thơ, điểm đến là Bến xe phía bắc Đông Hà vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a77",
        "toStation": "619f58f3e763b3374c7a79dd",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20d8e",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phan Thiết, điểm đến là Bến xe Điện Biên vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7a",
        "toStation": "619f58f3e763b3374c7a7a70",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20df0",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe TP Tuyên Quang, điểm đến là Bến xe Tam Kỳ vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7e",
        "toStation": "619f58f3e763b3374c7a7a5a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20dbf",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Lạng Sơn, điểm đến là Bến xe Hữu  nghị Quan vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7b",
        "toStation": "619f58f3e763b3374c7a7a26",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20e52",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía Nam Thừa Thiên Huế, điểm đến là Bến xe Nghĩa Đàn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7d",
        "toStation": "619f58f3e763b3374c7a7a32",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20c68",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Điện Biên, điểm đến là Bến xe Phía Nam TP Nam Định vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a70",
        "toStation": "619f58f3e763b3374c7a798c",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20e83",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phía Nam Buôn Ma Thuột, điểm đến là Bến xe Văn Bàn vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7f",
        "toStation": "619f58f3e763b3374c7a79ce",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20e21",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đồng Hới, điểm đến là Bến xe Lạc Long vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a7c",
        "toStation": "619f58f3e763b3374c7a79c0",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20ee5",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phía Nam Nha Trang, điểm đến là Bến Xe Phước Long (Bình Phước) vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a81",
        "toStation": "619f58f3e763b3374c7a7a4e",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20f16",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ba Hòn, điểm đến là Bến xe Thảo Quyên vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a82",
        "toStation": "619f58f3e763b3374c7a7a73",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20fa9",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Quy Nhơn, điểm đến là Bến xe Bến Trại vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a85",
        "toStation": "619f58f3e763b3374c7a7951",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20f78",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hưng Yên, điểm đến là Bến xe Cẩm Khê vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a84",
        "toStation": "619f58f3e763b3374c7a796c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2106d",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe huyện Đắk Đoa, điểm đến là Bến xe Ba Đồn vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a89",
        "toStation": "619f58f3e763b3374c7a7a6b",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2103c",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe An Khê, điểm đến là Bến xe Cao Bằng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a87",
        "toStation": "619f58f3e763b3374c7a7aa0",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20eb4",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đắk Hà, điểm đến là Bến xe Đức Phổ vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a80",
        "toStation": "619f58f3e763b3374c7a7987",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20f47",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vạn Giã, điểm đến là Bến Xe Chợ Lớn vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a83",
        "toStation": "619f58f3e763b3374c7a7a18",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c20fda",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Liên Tỉnh Đắk Lắk, điểm đến là Bến xe Cái Nước vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a86",
        "toStation": "619f58f3e763b3374c7a79b1",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2100b",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Chư Sê, điểm đến là Bến xe Đồng Hới vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a88",
        "toStation": "619f58f3e763b3374c7a7a7c",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2109e",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Quang Vinh, điểm đến là Bến xe Pác Nặm vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8c",
        "toStation": "619f58f3e763b3374c7a7a08",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21131",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nam Hải, điểm đến là Bến Xe Bến Tre vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8d",
        "toStation": "619f58f3e763b3374c7a793f",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21100",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Buôn Hồ, điểm đến là Bến xe Cẩm Thủy vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8b",
        "toStation": "619f58f3e763b3374c7a79ed",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21162",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tuấn Hoa, điểm đến là Bến xe Ngã Tư Ga vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8e",
        "toStation": "619f58f3e763b3374c7a7aa6",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21193",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thường Xuân, điểm đến là Bến xe Đồng Nai vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8f",
        "toStation": "619f58f3e763b3374c7a7ab4",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c211c4",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Lộc Ninh, điểm đến là Bến xe Gò Công vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a94",
        "toStation": "619f58f3e763b3374c7a7a3c",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c210cf",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Khách Bảo Lộc, điểm đến là Bến xe Niệm Nghĩa vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a8a",
        "toStation": "619f58f3e763b3374c7a7a3b",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21288",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ea HLeo, điểm đến là Bến Xe Hạ Hòa vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a97",
        "toStation": "619f58f3e763b3374c7a79d3",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c211f5",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mỹ Quới, điểm đến là Bến Xe Phố Lu vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a90",
        "toStation": "619f58f3e763b3374c7a799e",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21226",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe An Phú, điểm đến là Bến xe Tà Lùng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a92",
        "toStation": "619f58f3e763b3374c7a7997",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21257",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phú Lộc, điểm đến là Bến xe Đức Long Bảo Lộc vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a91",
        "toStation": "619f58f3e763b3374c7a7a39",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c212b9",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe thành phố Hà Giang, điểm đến là Bến xe Đồng Nai vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a93",
        "toStation": "619f58f3e763b3374c7a7ab4",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2131b",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe thị xã Lai Châu, điểm đến là Bến xe Vĩnh Lộc vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a95",
        "toStation": "619f58f3e763b3374c7a79ee",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2134c",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phú Thọ, điểm đến là Bến xe huyện Sơn Hòa vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a96",
        "toStation": "619f58f3e763b3374c7a7988",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2137d",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Thái Thụy, điểm đến là Bến xe Đức Long Gia Lai vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9a",
        "toStation": "619f58f3e763b3374c7a7a3d",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c212ea",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe phía nam TP Huế, điểm đến là Bến xe Quy Đạt vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a98",
        "toStation": "619f58f3e763b3374c7a79d6",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c213ae",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Phù Mỹ, điểm đến là Bến xe TP Huế vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a99",
        "toStation": "619f58f3e763b3374c7a79f1",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c213df",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Mũi Né, điểm đến là Bến Xe Khách Nghi Sơn vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9b",
        "toStation": "619f58f3e763b3374c7a7947",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21410",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Nam Phan Thiết, điểm đến là Bến Xe Gia Bình vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9f",
        "toStation": "619f58f3e763b3374c7a79f9",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21441",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cao Bằng, điểm đến là Bến Xe Gò Bồi vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa0",
        "toStation": "619f58f3e763b3374c7a7a6c",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c214a3",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Đồng Xoài, điểm đến là Bến xe tỉnh Lai Châu vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9d",
        "toStation": "619f58f3e763b3374c7a7a53",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21472",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đại Lộc, điểm đến là Bến Xe Ngọc Lặc vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9c",
        "toStation": "619f58f3e763b3374c7a7a05",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c214d4",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Bắc Phan Thiết, điểm đến là Bến xe Phan Rí Cửa vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7a9e",
        "toStation": "619f58f3e763b3374c7a79af",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21505",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cà Mau, điểm đến là Bến Xe Uông Bí vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa1",
        "toStation": "619f58f3e763b3374c7a7982",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21536",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tam Hiệp, điểm đến là Bến Xe Quảng Xương vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa2",
        "toStation": "619f58f3e763b3374c7a79fd",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21567",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Gia Nghĩa, điểm đến là Bến xe Hàm Yên vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa3",
        "toStation": "619f58f3e763b3374c7a79e6",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21598",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Điện Biên Phủ, điểm đến là Bến Xe Tịnh Biên vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa4",
        "toStation": "619f58f3e763b3374c7a7975",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c215c9",
        "image": [
            "https://static.vexere.com/production/images/1610426896200.jpeg",
            "https://static.vexere.com/production/images/1602045276651.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cao Lãnh, điểm đến là Bến xe Nam Hải vào Sáng Thứ Hai hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa5",
        "toStation": "619f58f3e763b3374c7a7a8d",
        "price": 150000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2162b",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Hải Dương, điểm đến là Bến xe khách Đà Nẵng vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa7",
        "toStation": "619f58f3e763b3374c7a7985",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2168d",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Tiền Giang, điểm đến là Bến Xe Tam Quan vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aaa",
        "toStation": "619f58f3e763b3374c7a794a",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c215fa",
        "image": [
            "https://static.vexere.com/production/images/1602045276651.jpeg",
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ngã Tư Ga, điểm đến là Bến Xe Gò Bồi vào Trưa Thứ Ba hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa6",
        "toStation": "619f58f3e763b3374c7a7a6c",
        "price": 200000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c216ef",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Sa Thầy, điểm đến là Bến Xe Ngọc Lặc vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aad",
        "toStation": "619f58f3e763b3374c7a7a05",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21720",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Vĩnh Yên, điểm đến là Bến xe Hải Hà vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aab",
        "toStation": "619f58f3e763b3374c7a7989",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2165c",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Cầu Rào, điểm đến là Bến Xe Lộc Ninh vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa8",
        "toStation": "619f58f3e763b3374c7a7a94",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c216be",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Long An, điểm đến là Bến xe Sa Thầy vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aa9",
        "toStation": "619f58f3e763b3374c7a7aad",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21751",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Ngọc Hồi, điểm đến là Đối diện bến xe Sapa vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aac",
        "toStation": "619f58f3e763b3374c7a799d",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21782",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Chợ Lách, điểm đến là Bến xe Trực Ninh vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab0",
        "toStation": "619f58f3e763b3374c7a795b",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c217b3",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe EaKar, điểm đến là Bến Xe Trân Bảo Trân vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aae",
        "toStation": "619f58f3e763b3374c7a7a12",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c217e4",
        "image": [
            "https://static.vexere.com/production/images/1610099185801.jpeg",
            "https://static.vexere.com/production/images/1614156656852.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Ba Tri, điểm đến là Bến xe Tây Ninh vào Sáng Sớm Thứ Sáu hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab1",
        "toStation": "619f58f3e763b3374c7a7a41",
        "price": 350000,
        "startTime": "2022-01-30T20:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21815",
        "image": [
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Thảo Châu, điểm đến là Bến Xe Hiệp Hòa vào Trưa Chủ Nhật hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7aaf",
        "toStation": "619f58f3e763b3374c7a7967",
        "price": 500000,
        "startTime": "2022-01-31T05:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21877",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Trị An, điểm đến là Bến xe Kiên Giang vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab2",
        "toStation": "619f58f3e763b3374c7a796e",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c218a8",
        "image": [
            "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
            "https://static.vexere.com/production/images/1592302526499.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Đồng Nai, điểm đến là Bến xe Thanh Ba vào Chiều Thứ Tư hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab4",
        "toStation": "619f58f3e763b3374c7a7a75",
        "price": 250000,
        "startTime": "2022-01-31T09:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c21846",
        "image": [
            "https://static.vexere.com/production/images/1614156656852.jpeg",
            "https://static.vexere.com/production/images/1599640577286.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Yên Nghĩa, điểm đến là Bến xe Nga Sơn vào Sáng Thứ Bảy hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab5",
        "toStation": "619f58f3e763b3374c7a79ef",
        "price": 400000,
        "startTime": "2022-01-31T00:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c218d9",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến xe Kim Sơn, điểm đến là Bến xe Mường La vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab3",
        "toStation": "619f58f3e763b3374c7a7a6a",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    },
    {
        "_id": "61a18f2ad8dd1716a0c2190a",
        "image": [
            "https://static.vexere.com/production/images/1592302526499.jpeg",
            "https://static.vexere.com/production/images/1610099185801.jpeg"
        ],
        "description": "Xe khách xuất phát từ Bến Xe Long Xuyên, điểm đến là Bến xe Điện Biên vào Tối Thứ Năm hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789",
        "acrossStation": [],
        "fromStation": "619f58f3e763b3374c7a7ab6",
        "toStation": "619f58f3e763b3374c7a7a70",
        "price": 300000,
        "startTime": "2022-01-31T14:00:00.000Z"
    }
]

const randomDate = (start, end) => {
    let a =  new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    a.setHours(0,0,0)
    return a
}

const seatCode = new Array(seat16, seat29, seat45 )
const generateTrip = async() => {
    try {
        let count = 0;
        // const trip = await Trip.find().select('_id image description acrossStation fromStation toStation price startTime').lean()
        // const content = JSON.stringify(trip)
        // fs.writeFile('./seedData/data/trip.json', content, err => {
        //     if (err) {
        //       console.error(err)
        //       return
        //     }
        // })

        // data.forEach(async(trip, index) => {
        //     count ++;
        //     const newTrip = new Trip({
        //         _id: trip._id,
        //         coach: coachData[index % 150]._id,
        //         description: trip.description,
        //         fromStation: trip.fromStation,
        //         toStation: trip.toStation,
        //         acrossStation: trip.acrossStation,
        //         startTime: new Date(trip.startTime),
        //         price: trip.price
        //     })
        //     const a = (coachData[index % 150].numberSeat == 16) ? 0 : (coachData[index % 150].numberSeat == 29) ? 1 : 2
        //     seatCode[a].forEach((code) => {
        //         const newSeat = new Seat({ code });
        //         newTrip.seats.push(newSeat);
        //     });
        //     await newTrip.save()
        // })

        const trips = await Trip.find({startTime: {$gte: new Date()}});
        console.log(trips.length)
        // await Promise.all(trips.map(async(trip) => {
        //     await Trip.findOneAndUpdate(
        //         {_id: trip._id},
        //         {startTime: randomDate(new Date(2021,11,1), new Date(2021,11,31))}
        //     )
        //     count ++;
        // }))
        
        console.log(`update ${count} Trip Success`);
    } catch (error) {
        throw new Error(error.message);
    }
};
module.exports = {
    generateTrip
}