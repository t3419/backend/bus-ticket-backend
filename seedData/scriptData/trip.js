const {data} = require('./station')
const {Seat} = require('../models/Seat')
const Trip = require('../models/Trip')
const image = [
                "https://static.vexere.com/production/images/1610426896200.jpeg",
                "https://static.vexere.com/production/images/1602045276651.jpeg",
                "https://static.vexere.com/c/i/20477/xe-thien-truong-(vinh-yen)-VeXeRe-xgz36Xs-1000x600.jpeg",
                "https://static.vexere.com/production/images/1592302526499.jpeg",
                "https://static.vexere.com/production/images/1610099185801.jpeg",
                "https://static.vexere.com/production/images/1614156656852.jpeg",
                "https://static.vexere.com/production/images/1599640577286.jpeg"
                ]
const day = ['Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy', 'Chủ Nhật'];
const price= [150000, 200000, 250000, 300000, 350000, 400000, 500000]
const timeOfDay = ['Sáng', 'Trưa', 'Chiều', 'Tối', 'Sáng Sớm'];
const timeStart = [7,12,16,21,3];
const description = (start, end, day, time) => {
    return `Xe khách xuất phát từ ${start}, điểm đến là ${end} vào ${time} ${day} hàng tuần.\n Xe được trang bị nước sát khuẩn và khẩu trang y tế nhằm đảm bảo an toàn trong mùa dịch covid-19. \n SĐT liên hệ: 0123456789`
}
const randomIntFromInterval = (min, max) => { 
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const seatCodes = [
    "A01",
    "A02",
    "A03",
    "A04",
    "A05",
    "A06",
    "A07",
    "A08",
    "A09",
    "A10",
    "A11",
    "A12",
    "B01",
    "B02",
    "B03",
    "B04",
    "B05",
    "B06",
    "B07",
    "B08",
    "B09",
    "B10",
    "B11",
    "B12",
  ];
const generateTrip = async() => {
    try {
        let count = 0;
        data.forEach(async(station, index) => {
            const random = randomIntFromInterval(0, image.length -1);
            let station_end = randomIntFromInterval(0, data.length -1);
            while(station_end == index) {
                station_end = randomIntFromInterval(0, data.length -1);
            }
            count ++;
            const a = timeStart[random % timeStart.length];
            const newTrip = new Trip({
                image: image.slice(random, random+2),
                departure_schedule: new Array({
                    day: day[random],
                    timeOfDay: timeOfDay[random % timeOfDay.length]
                }),
                description: description(station.name, data[station_end].name, day[random], timeOfDay[random % timeOfDay.length]),
                fromStation: station._id,
                toStation: data[station_end]._id,
                price: price[random],
                startTime: new Date(2021, 12, 31, a, 0, 0)
            })
            seatCodes.forEach((code) => {
                const newSeat = new Seat({ code });
                newTrip.seats.push(newSeat);
            });
            await newTrip.save()
        })
        console.log(`Seed ${count} Trip Success`);
    } catch (error) {
        throw new Error(error.message);
    }
};
module.exports = {
    generateTrip
}