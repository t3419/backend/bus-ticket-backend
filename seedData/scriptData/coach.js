
const Coach = require('../../models/Coach')

const obj = {
    '16': {
        name: [ "Toyota Hiace", "Mercedes Sprinter", "Hyundai Solati"],
        image: [
            ["https://xehay.vn/uploads/images/2018/12/01/xehay-Toyota-HiAce-Preview%20(1).jpg"],
            ["https://static.carmudi.vn/wp-content/uploads/2021-01/YExditM8So.jpg"],
            ["https://dailyauto.vn/wp-content/uploads/2019/08/hyunai_solati_1.jpg"]
        ]
    },
    '29': {
        name: ["Hyundai County","Samco Felix", "Thaco Garden TB79s", "Thaco Meadow TB85s" ],
        image: [
            ["https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/xe-29-cho-county.jpg", "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Hyundai-County-Limousine.jpg"],
            ["https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Samco-Felix.jpg"],
            ["https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s-4.jpg", "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Garden-TB79s.jpg"],
            ["https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s.jpg", "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-1.jpg", "https://xevietanh.com/userfiles/images/Tin-Tuc/dich-vu-thue-xe-29-cho/Thaco-Meadow-TB85s-2.jpg"]
        ],
    },
    '45': {
        name: ["Hyundai Universe", "Hyundai Aero Space", "Thaco Universe"],
        image: [
            ["https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/Xe-Hyundai-Universe-45-cho%CC%82%CC%83.jpg"],
            ["https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Hyundai-Space-45-cho%CC%82%CC%83.jpg"],
            ["https://chothuexedulichtphcm.vn/wp-content/uploads/2020/05/xe-Universe-Thaco-45-cho%CC%82%CC%83.jpg"],
        ],
    },
}

const description = "Là chương trình bảo vệ an toàn cho hành khách sử dụng dịch vụ của LetGo trong mùa dịch Covid. LetGo đồng hành các nhà xe đối tác triển khai biện pháp bảo vệ an toàn cho hành khách, như sau (1) Kiểm tra thân nhiệt hành khách trước khi lên xe;(2) Trang bị nước rửa tay;(3) Có đảm bảo khuyến cáo tất cả hành khách đeo khẩu trang khi lên xe; (4) Có thực hiện khử trùng xe;(5) Tài xế và nhân viên đã được tiêm vắc xin"

const nha_xe = [ "Sao Việt", "Tiến Đạt", "Văn Minh", "Quang Nghị", "Hưng Long", "Xuân Trường", "Quốc Đạt", "Xuân Tráng Limousine","Thanh Bình Xanh", "Nam Á Châu", "Trọng Minh", "Thiện Thành limousine", "Khanh Phong",  "Minh Quốc", "Thuận Tiến" ]
const generateCoach = async() => {
    try {
        let count = 0;
        nha_xe.forEach(async(nhaxe) => {
            Object.keys(obj).forEach(async (type) => {
                obj[type].name.forEach(async (name, index) => {
                    count ++;
                    const newCoach = new Coach({
                        name: name,
                        image: obj[type].image[index],
                        coachOwner: nhaxe,
                        numberSeat: parseInt(type),
                        description,
                    })
                    await newCoach.save()
                })
            })
        })
        console.log(`Seed ${count} Coach Success`);
    } catch (error) {
        throw new Error(error.message);
    }
};