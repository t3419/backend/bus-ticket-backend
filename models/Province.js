const mongoose = require('mongoose');

const provinceSchema = new mongoose.Schema({
  code: {
    type: String,
   
    trim: true,
  },
  name: {
    type: String,
   
    trim: true,
  },
  type: {
    type: String,
  
  },
  nameWithType: {
    type: String,
   
    trim: true,
  },
});
// const Province = mongoose.model('Province', provinceSchema, 'Province');

// module.exports = { Province, provinceSchema };
const Province = mongoose.model('Province', provinceSchema);

module.exports = Province;
