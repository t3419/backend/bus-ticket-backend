const mongoose = require('mongoose');

const stationSchema = new mongoose.Schema({
  name: { type: String, required: true, trim: true },
  address: { type: String, required: true },
  province: { type: String, required: true, trim: true },
  district: {
    type: String,
    required: true,
    trim: true
  },
  image: {
    type: String,
    trim: true,
  }
});
const Station = mongoose.model('Station', stationSchema, 'Station');

module.exports = Station;
