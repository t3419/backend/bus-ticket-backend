const mongoose = require('mongoose');

const coachSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
        },
        image: [{
            type: String,
            default: ['https://cdnimg.vietnamplus.vn/t620/uploaded/natmjs/2014_10_02/giuong_nam.jpg'],
        }],
        coachOwner: {
            type: String,
            require:true,
            trim: true,
        },
        numberSeat: {
            type: Number,
            enum: [16, 29, 45]
        },
        description: {
            type: String,
            default: '',
            trim: true
        }
    },
    {timestamps: true}
);
const Coach = mongoose.model('Coach', coachSchema);

module.exports = Coach;
