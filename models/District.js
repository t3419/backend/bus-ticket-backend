const mongoose = require('mongoose');

const districtSchema = new mongoose.Schema({
  code: {
    type: String,
    trim: true,
  },
  parentCode: {
    type: String,
    require:true,
    trim: true,
  },
  name: {
    type: String,
    require:true,
    trim: true,
  },
  type: {
    type: String,
    require:true,
    trim: true,
  },
  nameWithType: {
    type: String,
    require:true,
    trim: true,
  },
  path: {
    type: String,
    require:true,
    trim: true,
  },
  pathWithType: {
    type: String,
    require:true,
    trim: true,
  },
});
const District = mongoose.model('District', districtSchema);

module.exports = District;
