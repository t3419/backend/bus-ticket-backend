const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema(
    {
        trip: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Trip',
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        coachOwner: {
            type: String,
            require:true,
            trim: true,
        },
        comment: {
            type: String,
            required: true,
            trim: true,
        },
        vote: {
            type: Number,
            enum: [1,2,3,4,5]
        }
    },
    {timestamps: true}
);

const Comment = mongoose.model('Comment', commentSchema, 'Comment');

module.exports = Comment;
