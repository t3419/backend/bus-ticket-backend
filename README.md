# Welcome to API LetGO

This API allowed users to search for trips, log in to select seats and book tickets.Receive information after successful booking via email

## Project using NodeJS, Express and MongoDB to manager data
## How to run

1. Clone: `https://gitlab.com/t3419/backend/bus-ticket-backend.git`
2. Run in terminal to start server: `yarn start:local`
3. Go to link : http://localhost:6789/docs/ to access Swagger interface
